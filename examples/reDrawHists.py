#!/usr/bin/env python

from qsquared import *

#Set up a new ROOT reader for a file
RT = RootInterface('histos.root','read')

#List the objects in the file
RT.ls()

#Read and draw a particular hist
# h = RT.readHist('ptpz')
# h.Draw('ptpz_draw.pdf',show=True)

# #Save all hists in the file to a new Q2 file
RT.reDrawHists()
