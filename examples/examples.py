from __future__ import division
from qsquared import *

VectorTests = False
HistogramTests = True

if VectorTests:
    print 'Vector Tests!'
    a = FourVector([5,1,1,1])#[5,1,1,1])
    print 'a:',a
    print '2*a:',2*a
    print 'a*2',a*2
    print a.m,'a mass'
    print a.m2,'a mass squared'
    print a*a,'a x a'
    print a**2,'a squared'
    print a/2,'a/2'
    print -a,'minus a'

    c = ThreeVector([1,2,3])
    print 'c:',c
    print '2*c',2*c
    print 'c*2',c*2
    print c.mag,'c magnitude'
    print c.unitVector,'c unit vector'
    print c*c,'c x c'
    print -c,'minus c'
    print c**2.0,'c squared'
    print c/2,'c/2'


if HistogramTests:
    print 'Histogram Tests!'

    # print '1D Histogram'
    # a = H1(25,-5,5)
    #
    # a.FillMany(np.random.normal(0,1,10000))
    # # a.Fill(-4.5,3000) #Add an extra point
    # print 'Number of entries:',a.nEntries
    # print 'Mean:',a.mean
    # print 'Std Dev:',a.stdev
    #
    # a.SetTitle("Normal Distribution")
    # a.SetXLabel("X axis label")
    # a.SetYLabel("Y axis label")
    # # a.SetLogY()
    # a.Draw("HistNormal.pdf",show=True)

    print '2D Histogram'
    b = H2(25,-5,5,25,-5,5)
    b.FillMany(np.random.normal(0,1,100000),np.random.normal(0,1,100000))
    print 'Mean X:',b.meanX
    print 'Std Dev X:',b.stdevX
    print 'Mean Y:',b.meanY
    print 'Std Dev Y:',b.stdevY

    b.SetTitle("2D Normal Distribution")
    b.SetXLabel("X axis label")
    b.SetYLabel("Y axis label")
    b.SetEqAspect()
    # b.SetLogZ()
    b.Draw("HistNormal2D",show=False)
