from qsquared import *

v = FourVector([5,1,1,1])
print 'Vector:',v

p = Particle(22,v)

print 'Particle:',p

e = Event()
e.AddParticle(p)
print 'Particles in event:',e.GetParticles()


g = G4Event()
g.SetWeight(1)
g.SetEventID(1)

t = G4Track()
h = G4Hit()

t.AddHit(h)
g.AddTrack(t)
