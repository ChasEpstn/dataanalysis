from qsquared import *

#Run convertRootHists.py before running this script!
#Open the file
QF = QFile('Q2Hists.qs')

#Get a list of hists in the file
hists = QF.GetStore('Histograms')
print hists.Tags()

#Get the hists and draw them
for hist in hists.Tags():
    print 'Reading hist {}'.format(hist)
    h = hists.Get(hist)
    h.Draw(hist) #By default does not show output
