import numpy as np
cimport numpy as np

cdef class H1C:
    cdef public double xMin
    cdef public double xMax
    cdef public int nBins
    cdef public double binWidth
    cdef public np.ndarray binCenters
    cdef public np.ndarray binVals
    cdef public np.ndarray binSumW2
    cdef public np.ndarray info
    def __init__(self, int nBins, double xMin, double xMax):
        self.xMin = xMin
        self.xMax = xMax
        self.nBins = nBins
        self.binWidth = (xMax - xMin) / nBins
        self.binCenters = (np.arange(nBins) + 0.5) * self.binWidth + self.xMin
        self.binVals = np.zeros(self.nBins, dtype=np.float64)
        self.binSumW2 = np.zeros(self.nBins, dtype=np.float64)
        self.info = np.zeros(4, dtype=np.float64)
    cpdef void Fill(self, double value, double weight = 1.0):
        cdef int bin_index = int((value - self.xMin) / self.binWidth)
        if 0 <= bin_index < self.nBins:
            self.binVals[bin_index] += weight
            self.binSumW2[bin_index] += weight**2
            self.info[0] += weight
            self.info[1] += value*weight
            self.info[2] += (value)**2*weight
            self.info[3] += 1
    cpdef void FillMany(self,values,weights = None):
        if weights is None:
            weights = np.ones(len(values))
        assert(len(values)==len(weights))
        for i in range(len(values)):
            self.Fill(values[i],weights[i])
    cpdef void ClearData(self):
        self.binVals = np.zeros(self.nBins, dtype=np.float64)
        self.binSumW2 = np.zeros(self.nBins, dtype=np.float64)
        self.info = np.zeros(4, dtype=np.float64)
    @property
    def nEntries(self):
        return np.int64(self.info[3])
    @property
    def mean(self):
        return self.info[1] / self.info[0]
    @property
    def stdev(self):
        return np.sqrt(self.info[2] / self.info[0] - self.mean**2)
    @property
    def data(self):
        return self.binVals
    @property
    def err(self):
        return np.sqrt(self.binSumW2)


cdef class H2C:
    cdef public double xMin
    cdef public double xMax
    cdef public int nBinsX
    cdef public double yMin
    cdef public double yMax
    cdef public int nBinsY
    cdef public double binWidthX
    cdef public double binWidthY
    cdef public np.ndarray binCentersX
    cdef public np.ndarray binCentersY
    cdef public np.ndarray binVals
    cdef public np.ndarray binSumW2
    cdef public np.ndarray info
    def __init__(self, int nBinsX, double xMin, double xMax, int nBinsY, double yMin, double yMax):
        self.xMin = xMin
        self.xMax = xMax
        self.nBinsX = nBinsX
        self.yMin = yMin
        self.yMax = yMax
        self.nBinsY = nBinsY
        self.binWidthX = (xMax - xMin) / nBinsX
        self.binWidthY = (yMax - yMin) / nBinsY
        self.binCentersX = (np.arange(nBinsX) + 0.5) * self.binWidthX + self.xMin
        self.binCentersY = (np.arange(nBinsY) + 0.5) * self.binWidthY + self.yMin
        self.binVals = np.zeros((self.nBinsX, self.nBinsY), dtype=np.float64)
        self.binSumW2 = np.zeros((self.nBinsX, self.nBinsY), dtype=np.float64)
        self.info = np.zeros(6, dtype=np.float64)
    cpdef void Fill(self, double valueX, double valueY, double weight=1.0):
        cdef int bin_indexX = int((valueX - self.xMin) / self.binWidthX)
        cdef int bin_indexY = int((valueY - self.yMin) / self.binWidthY)
        if 0 <= bin_indexX < self.nBinsX:
            if 0 <= bin_indexY < self.nBinsY:
                self.binVals[bin_indexX, bin_indexY] += weight
                self.binSumW2[bin_indexX, bin_indexY] += weight**2
                self.info[0] += weight
                self.info[1] += valueX*weight
                self.info[2] += (valueX)**2*weight
                self.info[3] += valueY*weight
                self.info[4] += (valueY)**2*weight
                self.info[5] += 1
    cpdef void FillMany(self,np.ndarray valuesX,np.ndarray valuesY, weights = None):
        if weights is None:
            weights = np.ones(len(valuesX),dtype=np.float64)
        assert(len(valuesX)==len(valuesY))
        assert(len(valuesX)==len(weights))
        for i in range(len(valuesX)):
            self.Fill(valuesX[i],valuesY[i],weights[i])
    cpdef void FillCross(self,np.ndarray xVals, np.ndarray yVals, double weight):#specifically set up for my detector tasks
        for xVal in xVals:
            for yVal in yVals:
                self.Fill(xVal,yVal,weight)
    #I hope these (below) are right!
    @property
    def nEntries(self):
        return np.int64(self.info[5])
    @property
    def meanX(self):
        return self.info[1] / self.info[0]
    @property
    def stdevX(self):
        return np.sqrt(self.info[2] / self.info[0] - self.meanX**2)
    @property
    def meanY(self):
        return self.info[3] / self.info[0]
    @property
    def stdevY(self):
        return np.sqrt(self.info[4] / self.info[0] - self.meanY**2)

# cdef class G4Event:
#     cdef public int eventID
#     cdef public double weight
#     cdef public str baseGeometry
#     cdef public str generatorName
#     cdef public tuple tracks
#     def __init__(self):
#         self.eventID = -1
#         self.weight = 0.0
#         self.baseGeometry = ''
#         self.generatorName = ''
#         self.tracks = ()
#     def GetEventID(self):
#         return self.eventID
#     def SetEventID(self, id):
#         self.eventID = id
#     def GetWeight(self):
#         return self.weight
#     def SetWeight(self, wght):
#         self.weight = wght
#     def SetBaseGeometry(self,geomX):
#         self.baseGeometry = geomX
#     def matchBaseGeometry(self, geomX):
#         return self.baseGeometry == geomX
#     def SetGeneratorName(self, physGenX):
#         self.generatorName = physGenX
#     def GetTracks(self):
#         return self.tracks
#     def AddTrack(self,track):
#         # track.Done()
#         # self.tracks.append(track)
#         self.tracks += (track,)
#     def GetNumTracks(self):
#         return len(self.tracks)
#     def GetTrack(self, t):
#         return self.tracks[t]
#     def __getstate__(self):
#         # Copy the object's state from self.__dict__ which contains
#         # all our instance attributes. Always use the dict.copy()
#         # method to avoid modifying the original state.
#         state = (self.eventID,self.weight,self.baseGeometry,self.generatorName,self.tracks,)
#         return state
#
#     def __setstate__(self, state):
#         # Restore instance attributes (i.e., filename and lineno).
#         eventID,weight,baseGeometry,generatorName,tracks = state
#         self.eventID = eventID
#         self.weight = weight
#         self.baseGeometry = baseGeometry
#         self.generatorName = generatorName
#         self.tracks = tracks
#         # pass
