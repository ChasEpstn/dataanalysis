import sys

def patchZODB(path):

    with open(path,'r') as file:
        data = file.readlines()

    assert('_protocol' in data[36])
    data[36] = data[36].replace('1','2')

    with open(path,'w') as file:
        file.writelines(data)

if __name__ == "__main__":
    p = sys.argv[1].replace('pyc','py')
    print p
    patchZODB(p)
