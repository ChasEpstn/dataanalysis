import numpy as np
cimport numpy as np

cdef class ThreeVector(object):
    cdef public np.ndarray q
    cpdef double magnitude(self)

cdef class FourVector(object):
    cdef public np.ndarray q
