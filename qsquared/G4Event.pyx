from Vectors cimport *
import numpy as np
cimport numpy as np

cdef class G4Event:
    def __init__(self):
        self.eventID = -1
        self.weight = 0
        self.baseGeometry = ''
        self.generatorName = ''
        self.tracks = []
    cpdef int GetEventID(self):
        return self.eventID
    cpdef void SetEventID(self, int id):
        self.eventID = id
    cpdef double GetWeight(self):
        return self.weight
    cpdef void SetWeight(self, double wght):
        self.weight = wght
    cpdef void SetBaseGeometry(self, str geomX):
        self.baseGeometry = geomX
    def matchBaseGeometry(self, geomX):
        return self.baseGeometry == geomX
    cpdef void SetGeneratorName(self, str physGenX):
        self.generatorName = physGenX
    cpdef list GetTracks(self):
        return self.tracks
    cpdef void AddTrack(self,G4Track track):
        self.tracks.append(track)
    cpdef int GetNumTracks(self):
        return len(self.tracks)
    cpdef G4Track GetTrack(self, int t):
        return self.tracks[t]
    def __getstate__(self):
        state = (self.eventID,self.weight,self.baseGeometry,self.generatorName,self.tracks,)
        return state
    def __setstate__(self, state):
        eventID,weight,baseGeometry,generatorName,tracks = state
        self.eventID = eventID
        self.weight = weight
        self.baseGeometry = baseGeometry
        self.generatorName = generatorName
        self.tracks = tracks

cdef class G4Track:
    def __init__(self):
        self.trackID = 0
        self.parentTrackID = 0
        self.origin = (0,0,0)
        self.particleName = ''
        self.momentumP4 = (0,0,0,0) # 4-momentum vector in MeV
        self.hits = []
    cpdef int GetTrackID(self):
        return self.trackID
    cpdef void SetTrackID(self, int t):
        self.trackID = t
    cpdef str GetParticleName(self):
        return self.particleName
    cpdef void SetParticleName(self, str p):
        self.particleName = p
    cpdef int GetParentID(self):
        return self.parentTrackID
    cpdef void SetParentID(self, int pp):
        self.parentTrackID = pp
    cpdef ThreeVector GetOrigin(self):
        return ThreeVector(self.origin)
    def SetOrigin(self, xyz):
        self.origin = tuple(xyz)
    cpdef ThreeVector Get3Momentum(self):
        return self.Get4Momentum().ThreeVector
    cpdef FourVector Get4Momentum(self):
        return FourVector(self.momentumP4)
    def Set4Momentum(self, p4):
        self.momentumP4 = tuple(p4)
    cpdef list GetHits(self):
        return self.hits
    cpdef void AddHit(self, G4Hit h):
        self.hits.append(h)
    def __getstate__(self):
        state = (self.trackID,self.parentTrackID,self.origin,self.particleName,self.momentumP4,self.hits,)
        return state
    def __setstate__(self, state):
        trackID,parentTrackID,origin,particleName,momentumP4,hits = state
        self.trackID = trackID
        self.parentTrackID = parentTrackID
        self.origin = origin
        self.particleName = particleName
        self.momentumP4 = momentumP4
        self.hits = hits

cdef class G4Hit:
    def __init__(self):
        self.position = (0,0,0) #global pos
        self.localPosition = (0,0,0)
        self.momentumP3 = (0,0,0)
        self.dE = 0
        self.kinE = 0
        self.detectorID = ''
    cpdef str GetDetectorID(self):
        return self.detectorID
    cpdef void SetDetectorID(self, str d):
        self.detectorID = d
    cpdef ThreeVector GetPosition(self):
        return ThreeVector(self.position)
    def SetPosition(self, xyz):
        self.position = tuple(xyz)
    cpdef ThreeVector GetLocalPosition(self):
        return ThreeVector(self.localPosition)
    def SetLocalPosition(self, xyz):
        self.localPosition = tuple(xyz)
    cpdef ThreeVector Get3Momentum(self):
        return ThreeVector(self.momentumP3)
    def Set3Momentum(self, p3):
        self.momentumP3 = tuple(p3)
    cpdef double GetDE(self):
        return self.dE
    cpdef void SetDE(self, double e):
        self.dE = e
    cpdef double GetKinE(self):
        return self.kinE
    cpdef void SetKinE(self, double e):
        self.kinE = e
    def __getstate__(self):
        state = (self.position,self.localPosition,self.momentumP3,self.dE,self.kinE,self.detectorID,)
        return state
    def __setstate__(self, state):
        position,localPosition,momentumP3,dE,kinE,detectorID = state
        self.position = position
        self.localPosition = localPosition
        self.momentumP3 = momentumP3
        self.dE = dE
        self.kinE = kinE
        self.detectorID = detectorID

# cdef class G4VertexC:
#     def __init__(self,xyz=[0,0,0]):
#         self.position = tuple(xyz)
#     def SetPosition(self,xyz):
#         self.position = tuple(xyz)
#     def GetPosition(self):
#         return ThreeVector(self.position)
