from __future__ import division
import numpy as np
import Gnuplot as gp
import os
from HistsC import H1C, H2C
# import matplotlib.pyplot as plt

class H1(H1C):
    title = None
    xlabel = None
    ylabel = None
    logy = False
    boxes = False
    yDrawMin = None
    yDrawMax = None
    xDrawMin = None
    xDrawMax = None
    legend = None
    def SetLegend(self,legend):
        self.legend = legend
    def Scale(self,scale):
        self.binVals *= scale
        self.binSumW2 *= scale**2.0
        self.info[0:3] *= scale
    def SetXRange(self,xMin,xMax):
        self.xDrawMin = xMin
        self.xDrawMax = xMax
    def SetYRange(self,yMin,yMax):
        self.yDrawMin = yMin
        self.yDrawMax = yMax
    def GPData(self):
        x = np.copy(self.binCenters)
        y = np.copy(self.binVals)
        y[y==0] = np.nan
        yErr = np.sqrt(self.binSumW2)
        if self.frac == -1:
            color = 'rgb "#2F4F4F"'
        elif self.frac == -2:
            color = 'rgb "black"'
        else:
            color = 'palette frac {}'.format(self.frac)
        if self.boxes:
            xErr = (self.binCenters[1]-self.binCenters[0])/2.0 * np.ones(len(self.binCenters))
            return gp.Data(x,y,xErr,yErr,with_='boxxyerrorbars lc {}'.format(color),title=self.legend)
        else:
            return gp.Data(x,y,yErr,with_='errorbars pt 7 ps 0.5 lc {}'.format(color),title=self.legend)
    def MakeCanvas(self):
        c = Canvas()
        c.Add(self)
        c.SetXLabel(self.xlabel)
        c.SetYLabel(self.ylabel)
        c.SetTitle(self.title)
        if self.logy:
            c.SetLogY()
        if self.yDrawMin:
            c.SetYRange(self.yDrawMin,self.yDrawMax)
        if self.xDrawMin:
            c.SetXRange(self.xDrawMin,self.xDrawMax)
        return c
    def Draw(self,fileName,show=False):
        c = self.MakeCanvas()
        c.Draw(fileName,show)
    def SetFromData(self, binCenters, binVals, binSumW2, info, title, xlabel, ylabel):
        self.binCenters = binCenters
        self.binVals = binVals
        self.binSumW2 = binSumW2
        self.xMin = self.binCenters[0]
        self.xMax = self.binCenters[-1]
        self.nBins = len(self.binCenters)
        self.binWidth = self.binCenters[1] - self.binCenters[0]
        self.info = info
        self.title = title
        self.xlabel = xlabel
        self.ylabel = ylabel
    def SetLogY(self,val=True):
        self.logy = val
    def SetBoxes(self,val=True):
        self.boxes = val
    def SetTitle(self,title):
        self.title = title
    def SetXLabel(self,xlabel):
        self.xlabel = xlabel
    def SetYLabel(self,ylabel):
        self.ylabel = ylabel
    # def __getstate__(self):
    #     state = (self.binCenters,self.binVals,self.binSumW2,self.info,self.title,self.xlabel,self.ylabel)
    #     return state
    # def __setstate__(self, state):
    #     # print state
    #     binCenters,binVals,binSumW2,info,title,xlabel,ylabel = state
    #     self.SetFromData(binCenters,binVals,binSumW2,info,title,xlabel,ylabel)
    def GetROOT(self):
        import ROOT as r
        newhist = r.TH1D(str(self.title)+'ROOT',str(self.title)+'ROOT',self.nBins,self.xMin,self.xMax)
        for i in range(self.binVals.shape[0]):
            newhist.SetBinContent(i+1,self.binVals[i])
            newhist.SetBinError(i+1,np.sqrt(self.binSumW2[i]))
        return newhist


class H2(H2C):
    title = None
    xlabel = None
    ylabel = None
    logz = False
    sq = False
    legend = None
    def SetLegend(self,legend):
        self.legend = legend
    def Scale(self,scale):
        self.binVals *= scale
        self.binSumW2 *= scale**2.0
    def SetFromData(self,binCentersX,binCentersY,binVals,binSumW2,info,title,xlabel,ylabel):
        self.binCentersX = binCentersX
        self.binCentersY = binCentersY
        self.binVals = binVals
        self.binSumW2 = binSumW2
        self.xMin = self.binCentersX[0]
        self.xMax = self.binCentersX[-1]
        self.nBinsX = len(self.binCentersX)
        self.binWidthX = self.binCentersX[1] - self.binCentersX[0]
        self.yMin = self.binCentersY[0]
        self.yMax = self.binCentersY[-1]
        self.nBinsY = len(self.binCentersY)
        self.binWidthY = self.binCentersY[1] - self.binCentersY[0]
        self.info = info
        self.title = title
        self.xlabel = xlabel
        self.ylabel = ylabel
    def GetSliceY(self):
        n = self.nBinsY
        min = self.yMin
        max = self.yMax
        h = H1(n,min,max)
        r = self.binCentersX
        vals = self.binVals
        sliceDat = np.zeros(vals.shape[1])
        sliceErr = np.zeros(vals.shape[1])
        for i in range(n):
            sliceDat[i] = np.ma.average(r,weights=vals[:,i])
            sliceErr[i] = np.ma.average((r-sliceDat[i])**2.0,weights=vals[:,i])
        h.binVals = sliceDat
        h.binSumW2 = sliceErr
        return h
    def GPData(self):
        x = self.binCentersX
        y = self.binCentersY
        z = np.copy(self.binVals)
        z[z==0] = np.nan
        return gp.GridData(z,x,y,with_='image pixels',binary=0,title=self.legend)
    def SetFont(self,f):
        self.font = '"{}"'.format(f)
    def MakeCanvas(self):
        c = Canvas()
        c.Add(self)
        c.SetXLabel(self.xlabel)
        c.SetYLabel(self.ylabel)
        c.SetTitle(self.title)
        if self.logz:
            c.SetLogZ()
        if self.sq:
            c.SetEqAspect()
        c.SetXRange(self.xMin,self.xMax)
        c.SetYRange(self.yMin,self.yMax)
        return c
    def Draw(self,fileName,show=False):
        c = self.MakeCanvas()
        c.Draw(fileName,show)
    def ProjectX(self):
        projHX = H1(self.nBinsX,self.xMin,self.xMax)
        projHX.binCenters = self.binCentersX
        projHX.binVals = self.binVals.sum(axis=1)
        projHX.binSumW2 = self.binSumW2.sum(axis=1)
        return projHX
    def ProjectY(self):
        projHY = H1(self.nBinsY,self.yMin,self.yMax)
        projHY.binCenters = self.binCentersY
        projHY.binVals = self.binVals.sum(axis=0)
        projHY.binSumW2 = self.binSumW2.sum(axis=0)
        return projHY
    def ClearData(self):
        self.binVals = np.zeros(self.nBins, dtype=np.float64)
        self.binSumW2 = np.zeros(self.nBins, dtype=np.float64)
        self.info = np.zeros(4, dtype=np.float64)
    def SetLogZ(self,val=True):
        self.logz = val
    def SetEqAspect(self,val=True):
        self.sq = val
    def SetTitle(self,title):
        self.title = title
    def SetXLabel(self,xlabel):
        self.xlabel = xlabel
    def SetYLabel(self,ylabel):
        self.ylabel = ylabel
    @property
    def data(self):
        return self.binVals
    @property
    def err(self):
        return np.sqrt(self.binSumW2)
    # def __getstate__(self):
    #     state = (self.binCentersX,self.binCentersY,self.binVals,self.binSumW2,self.info,self.title,self.xlabel,self.ylabel)
    #     return state
    # def __setstate__(self, state):
    #     binCentersX,binCentersY,binVals,binSumW2,info,title,xlabel,ylabel = state
    #     self.SetFromData(binCentersX,binCentersY,binVals,binSumW2,info,title,xlabel,ylabel)
    def GetROOT(self):
        import ROOT as r
        newhist = r.TH2D(str(self.title)+'ROOT',str(self.title)+'ROOT',self.nBinsX,self.xMin,self.xMax,self.nBinsY,self.yMin,self.yMax)
        for i in range(self.binVals.shape[0]):
            for j in range(self.binVals.shape[1]):
                newhist.SetBinContent(i+1,j+1,self.binVals[i,j])
                newhist.SetBinError(i+1,j+1,np.sqrt(self.binSumW2[i,j]))
        return newhist


#This is a plot of x/y points with x/y errorbars
class Series:
    def __init__(self):
        self.X = []
        self.Y = []
        self.XERR = []
        self.YERR = []
        self.title = None
        self.xlabel = None
        self.ylabel = None
        self.logy = False
        self.yDrawMin = None
        self.yDrawMax = None
        self.xDrawMin = None
        self.xDrawMax = None
        self.legend = None
        self.frac = -1.0
        # self.boxes = False
        self.withType = "xyerrorbars pt 7 ps 0.5"
    def AddPoint(self,x,y,xerr,yerr):
        self.X.append(x)
        self.Y.append(y)
        self.XERR.append(xerr)
        self.YERR.append(yerr)
    def SetBoxes(self,val=True):
        # self.boxes = val
        self.withType = "boxxyerrorbars"
    def SetWithType(self,wt):
        self.withType = wt
    def SetLegend(self,legend):
        self.legend = legend
    def Scale(self,scale):
        self.Y = [scale*i for i in self.Y]
        self.YERR = [scale*i for i in self.YERR]
    def SetXRange(self,xMin,xMax):
        self.xDrawMin = xMin
        self.xDrawMax = xMax
    def SetYRange(self,yMin,yMax):
        self.yDrawMin = yMin
        self.yDrawMax = yMax
    def GPData(self):
        if self.frac == -1:
            return gp.Data(self.X,self.Y,self.XERR,self.YERR,with_='{} lc rgb "#2F4F4F"'.format(self.withType),title=self.legend)
        if self.frac == -2:
            return gp.Data(self.X,self.Y,self.XERR,self.YERR,with_='{} lc rgb "black"'.format(self.withType),title=self.legend)
        else:
            return gp.Data(self.X,self.Y,self.XERR,self.YERR,with_='{} lc palette frac {}'.format(self.withType,self.frac),title=self.legend)
    def SetFont(self,f):
        self.font = '"{}"'.format(f)
    def MakeCanvas(self):
        c = Canvas()
        c.Add(self)
        c.SetXLabel(self.xlabel)
        c.SetYLabel(self.ylabel)
        c.SetTitle(self.title)
        if self.logy:
            c.SetLogY()
        if self.yDrawMin:
            c.SetYRange(self.yDrawMin,self.yDrawMax)
        if self.xDrawMin:
            c.SetXRange(self.xDrawMin,self.xDrawMax)
        return c
    def Draw(self,fileName,show=False):
        c = self.MakeCanvas()
        c.Draw(fileName,show)
    def SetLogY(self):
        self.logy = True
    def SetTitle(self,title):
        self.title = title
    def SetXLabel(self,xlabel):
        self.xlabel = xlabel
    def SetYLabel(self,ylabel):
        self.ylabel = ylabel

#This is a plot of x/y points with a line
class Line:
    def __init__(self):
        self.X = []
        self.Y = []
        self.title = None
        self.xlabel = None
        self.ylabel = None
        self.logy = False
        self.yDrawMin = None
        self.yDrawMax = None
        self.xDrawMin = None
        self.xDrawMax = None
        self.legend = None
        self.frac = -1.0
        self.bold = 0
        # self.withAdd = ""
        self.withType = "lines"
    def AddPoint(self,x,y):
        self.X.append(x)
        self.Y.append(y)
    def SetWithType(self,wt):
        self.withType = wt
    def SetLinesPoints(self):
        self.withType = "linespoints ls 1"
    def SetGhost(self):
        self.bold = -2
    def Scale(self,scale):
        self.Y = [scale*i for i in self.Y]
    def SetLegend(self,legend):
        self.legend = legend
    def SetXRange(self,xMin,xMax):
        self.xDrawMin = xMin
        self.xDrawMax = xMax
    def SetYRange(self,yMin,yMax):
        self.yDrawMin = yMin
        self.yDrawMax = yMax
    def GPData(self):
        if self.bold == 1:
            bold = 'lw 8'
        elif self.bold == -1:
            bold = 'lw 2'
        elif self.bold == -2:
            bold = 'lw 0'
        else:
            bold = 'lw 4'
        if self.frac == -1:
            return gp.Data(self.X,self.Y,with_='{} {} lc rgb "#2F4F4F"'.format(self.withType,bold),title=self.legend)
        elif self.frac == -2:
            return gp.Data(self.X,self.Y,with_='{} {} lc rgb "black"'.format(self.withType,bold),title=self.legend)
        else:
            return gp.Data(self.X,self.Y,with_='{} {} lc palette frac {}'.format(self.withType,bold,self.frac),title=self.legend)
    def MakeCanvas(self):
        c = Canvas()
        c.Add(self)
        c.SetXLabel(self.xlabel)
        c.SetYLabel(self.ylabel)
        c.SetTitle(self.title)
        if self.logy:
            c.SetLogY()
        if self.yDrawMin:
            c.SetYRange(self.yDrawMin,self.yDrawMax)
        if self.xDrawMin:
            c.SetXRange(self.xDrawMin,self.xDrawMax)
        return c
    def Draw(self,fileName,show=False):
        c = self.MakeCanvas()
        c.Draw(fileName,show)
    def SetLogY(self):
        self.logy = True
    def SetBold(self):
        self.bold = 1
    def SetThin(self):
        self.bold = -1
    def SetTitle(self,title):
        self.title = title
    def SetXLabel(self,xlabel):
        self.xlabel = xlabel
    def SetYLabel(self,ylabel):
        self.ylabel = ylabel

class Canvas:
    def __init__(self):
        self.title = None
        self.xlabel = None
        self.ylabel = None
        self.logy = False
        self.yDrawMin = None
        self.yDrawMax = None
        self.xDrawMin = None
        self.xDrawMax = None
        self.legend = None
        self.logz = False
        self.sq = False
        self.plotObjs = []
        self.objTot = 0
        self.color = True
        self.cmds = []
        self.reverseCol = False
        self.font = 'Helvetica Neue'
        self.fontoptions = ''#'Scale=0.9'#'SizeFeatures={Size=10}'
        self.interval = None
        self.SetFont('Whitney Book','RawFeature=+ss14,RawFeature=+ss10')
        self.recolor = True
    def DontRecolor(self):
        self.recolor = False
    def GnuCommand(self,cmd):
        self.cmds.append(cmd)
    def SetInterval(self,intrvl):
        self.interval = intrvl
    def ReverseColors(self):
        self.reverseCol = True
    def SetXRange(self,xMin,xMax):
        self.xDrawMin = xMin
        self.xDrawMax = xMax
    def SetYRange(self,yMin,yMax):
        self.yDrawMin = yMin
        self.yDrawMax = yMax
    def SetBlack(self):
        self.color = False
    def SetColor(self):
        self.color = True
    def Add(self, obj):
        self.plotObjs.append(obj)
        self.objTot += 1
    def ClearAll(self):
        self.__init__()
    def ClearData(self):
        self.plotObjs = []
        self.objTot = 0
    def SetFont(self,f,opts=None):
        self.font = '{}'.format(f)
        if opts:
            self.fontoptions += ','+opts
    def Draw(self,fileName,show=False):
        lt = 0
        if self.recolor:
            if len(self.plotObjs) == 1:
                if self.color:
                    self.plotObjs[0].frac = -1
                else:
                    self.plotObjs[0].frac = -2
            else:
                for i in range(len(self.plotObjs)):
                    if not isinstance(self.plotObjs[i],H2):
                        if self.color:
                            if self.reverseCol:
                                self.plotObjs[i].frac = ((self.objTot-1-lt)/(self.objTot-1))*0.75
                            else:
                                self.plotObjs[i].frac = (lt/(self.objTot-1))*0.75
                            lt += 1
                        else:
                            self.plotObjs[i].frac = -2
        if not fileName.endswith('.tex'):
            fileName = fileName + '.tex'
        g = gp.Gnuplot()#debug=1)
        header  = r'"\\usepackage{{mathspec}}\\usepackage{{textgreek}}\\usepackage{{siunitx}}\\setallmainfonts[{}]{{{}}}'.format(self.fontoptions,self.font)
        # print 'HEADER',header
        g('set terminal cairolatex size 5in,3in fontscale 0.85 pdf standalone header ' + header )
        # g('set terminal svg size 500,300 fname {} fsize 14 rounded dashed'.format(self.font))
        # g('set terminal context standalone')
        g('set loadpath "{}/color-palettes/"'.format(os.path.dirname(os.path.realpath(__file__))))
        g('load "viridis.pal"')
        if isinstance(self.plotObjs[0],H2):
            g('load "cse.pal"')
            g('set palette negative')
            g('set tics front')
        if not isinstance(self.plotObjs[0],H2):
            g('unset colorbox')
            g('set tics nomirror')
            g('set border 3')
            g('set style fill transparent solid 0.75')
            g('set grid lt -1 dt ".."')
        if self.logy:
            g('set logscale y')
            g('set format y "$10^{%L}$"')
        if self.logz:
            g('set logscale cb')
            g('set format cb "$10^{%L}$"')
        # g('unset grid')
        g('set key invert spacing 1.25')# spacing 2.25')
        g('set xtics nomirror')
        g('set style line 11 lc rgb "#404040" lt 1 lw 2')
        g('set border back ls 11')
        # g('set ylabel offset -1')
        # g('set xlabel offset 0,-0.5')
        if self.yDrawMin:
            g('set yrange [{}:{}]'.format(self.yDrawMin,self.yDrawMax))
        if self.xDrawMin:
            g('set xrange [{}:{}]'.format(self.xDrawMin,self.xDrawMax))
        if self.sq:
            g('set size ratio -1')
        g.title(self.title)
        g.xlabel(self.xlabel)
        g.ylabel(self.ylabel)
        d = []
        for obj in self.plotObjs:
            d.append(obj.GPData())
        for cmd in self.cmds:
            g(cmd)
        g('set output "'+fileName+'"')
        if self.interval:
            g.plot(*d)
            g('set ytics {}'.format(self.interval))
            g('set yrange [GPVAL_Y_MIN:GPVAL_Y_MAX]')
            g('set output "'+fileName+'"')
        g.plot(*d)
        g('unset out')
        g('!xelatex {}'.format(fileName))
        g.close()
        fileName = fileName.replace('.pdf','')
        fileName = fileName.replace('.tex','')
        os.remove('{}-inc.pdf'.format(fileName))
        os.remove('{}.aux'.format(fileName))
        os.remove('{}.log'.format(fileName))
        os.remove('{}.tex'.format(fileName))
        if show:
            import subprocess
            subprocess.call(['open',fileName])
    def SetLogY(self):
        self.logy = True
    def SetTitle(self,title):
        if title:
            self.title = r'{{\\large {}}}'.format(title)
        else:
            self.title = None
    def SetXLabel(self,xlabel):
        self.xlabel = xlabel
    def SetYLabel(self,ylabel):
        self.ylabel = ylabel
    def SetLogZ(self):
        self.logz = True
    def SetEqAspect(self):
        self.sq = True

class FillLines(Line):
    def __init__(self,l1,l2):
        self.l1 = l1
        self.l2 = l2
        self.X = []
        self.Y = []
        self.title = None
        self.xlabel = None
        self.ylabel = None
        self.logy = False
        self.yDrawMin = None
        self.yDrawMax = None
        self.xDrawMin = None
        self.xDrawMax = None
        self.legend = None
        self.frac = -1.0
        self.bold = 0
    def GPData(self):
        # if self.bold == 1:
        #     bold = 'lw 8'
        # elif self.bold == -1:
        #     bold = 'lw 2'
        # else:
        #     bold = 'lw 4'
        # if self.frac == -1:
        #     return gp.Data(self.X,self.Y,with_='lines {} lc rgb "#2F4F4F"'.format(bold),title=self.legend)
        # elif self.frac == -2:
        #     return gp.Data(self.X,self.Y,with_='lines {} lc rgb "black"'.format(bold),title=self.legend)
        # else:
        #     return gp.Data(self.X,self.Y,with_='lines {} lc palette frac {}'.format(bold,self.frac),title=self.legend)
        return gp.Data(self.l1.X,self.l1.Y,self.l2.Y,with_="filledcu fillcolor palette frac 0.7")
