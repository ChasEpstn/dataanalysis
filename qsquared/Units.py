# -*- coding: utf-8 -*-
from __future__ import division
import numpy as np
from numbers import Number
import copy
import os
from string import maketrans

def sin(n):
    if isinstance(n,Number):
        return np.sin(n)
    else:
        newUnc = np.abs(np.cos(n.rad.val)*n.rad.err)
        return ValWithErr(np.sin(n.rad.val),newUnc)

def cos(n):
    if isinstance(n,Number):
        return np.cos(n)
    else:
        newUnc = np.abs(np.sin(n.rad.val)*n.rad.err)
        return ValWithErr(np.cos(n.rad.val),newUnc)

def tan(n):
    if isinstance(n,Number):
        return np.tan(n)
    else:
        newUnc = np.abs((1.0/np.cos(n.rad.val))**2*n.rad.err)
        return ValWithErr(np.tan(n.rad.val),newUnc)

def asin(n):
    if isinstance(n,CoreValWithErr):
        newUnc = np.abs(n.err/np.sqrt(1-n.val**2))
        return AngleUnit((np.arcsin(n.val),newUnc),'rad')
    elif isinstance(n,Number):
        return np.arcsin(n)
    else:
        return AngleUnit(np.arcsin(n),'rad')

def acos(n):
    if isinstance(n,CoreValWithErr):
        newUnc = np.abs(n.err/np.sqrt(1-n.val**2))
        return AngleUnit((np.arccos(n.val),newUnc),'rad')
    elif isinstance(n,Number):
        return np.arccos(n)
    else:
        return AngleUnit(np.arccos(n),'rad')

def atan(n):
    if isinstance(n,CoreValWithErr):
        newUnc = np.abs(n.err/(1+n.val**2))
        return AngleUnit((np.arctan(n.val),newUnc),'rad')
    elif isinstance(n,Number):
        return np.arctan(n)
    else:
        return AngleUnit(np.arctan(n),'rad')

def sqrt(n):
    return n**0.5


class CoreValWithErr:
    def __init__(self,coreVal,unc=0):
        self.coreVal = coreVal
        self.unc = unc
    def __add__(self,n):
        if isinstance(n,CoreValWithErr):
            newUnc = np.sqrt(n.err**2 + self.err**2)
            return CoreValWithErr(self.val+n.val,newUnc)
        elif isinstance(n,Number):
            return CoreValWithErr(self.val+n,self.err)
        else:
            raise TypeError('Adding something you cannot!')
    def __mul__(self,n):
        if isinstance(n,CoreValWithErr):
            newUnc = np.sqrt((self.val*n.err)**2 + (self.err*n.val)**2)
            return CoreValWithErr(self.val*n.val,newUnc)
        elif isinstance(n,Number):
            return CoreValWithErr(self.val*n, self.err*n)
        else:
            raise TypeError('Multiplying something you cannot!')
    def __rmul__(self,n):
        if isinstance(n,CoreValWithErr):
            newUnc = np.sqrt((self.val*n.err)**2 + (self.err*n.val)**2)
            return CoreValWithErr(self.val*n.val,newUnc)
        elif isinstance(n,Number):
            return CoreValWithErr(self.val*n, self.err*n)
        else:
            raise TypeError('Multiplying something you cannot!')
    def __sub__(self,n):
        if isinstance(n,CoreValWithErr):
            newUnc = np.sqrt(n.err**2 + self.err**2)
            return CoreValWithErr(self.val-n.val,newUnc)
        elif isinstance(n,Number):
            return CoreValWithErr(self.val-n,self.err)
        else:
            raise TypeError('Subtracting something you cannot!')
    def __radd__(self, n):
        if isinstance(n,CoreValWithErr):
            newUnc = np.sqrt(n.err**2 + self.err**2)
            return CoreValWithErr(self.val+n.val,newUnc)
        elif isinstance(n,Number):
            return CoreValWithErr(self.val+n,self.err)
        else:
            raise TypeError('Adding something you cannot!')
    def __rsub__(self, n):
        if isinstance(n,CoreValWithErr):
            newUnc = np.sqrt(n.err**2 + self.err**2)
            return CoreValWithErr(n.val-self.val,newUnc)
        elif isinstance(n,Number):
            return CoreValWithErr(n-self.val,self.err)
        else:
            raise TypeError('Subtracting something you cannot!')
    def __truediv__(self,n):
        if self is n:
            return 1
        if isinstance(n,CoreValWithErr):
            newUnc = np.sqrt((self.err/n.val)**2 + (self.val*n.err/n.val**2)**2)
            return CoreValWithErr(self.val/n.val, newUnc)
        elif isinstance(n,Number):
            return CoreValWithErr(self.val/n,self.err/n)
        else:
            raise TypeError('Dividing something you cannot!')
    def __div__(self,n):
        return self.__truediv__(n)
    def __pow__(self,pow):
        assert isinstance(pow,Number)
        newUnc = np.abs(pow*self.val**(pow-1)*self.err)
        return CoreValWithErr(self.val**pow,newUnc)
    def __rtruediv__(self, n):
        if isinstance(n,CoreValWithErr):
            newUnc = np.sqrt((n.err/self.val)**2 + (n.val*self.err/self.val**2)**2)
            return CoreValWithErr(n.val/self.val, newUnc)
        elif isinstance(n,Number):
            newUnc = np.sqrt((n*self.err/self.val**2)**2)
            return CoreValWithErr(n/self.val,newUnc)
        else:
            raise TypeError('Dividing something you cannot!')
    def __rdiv__(self, n):
        return self.__rtruediv__(n)
    def __neg__(self):
        return CoreValWithErr(-self.val, self.err)
    def __repr__(self):
        if self.err != 0:
            return '{} +- {}'.format(self.val,self.err)
        else:
            return str(self.val)
    @property
    def val(self):
        return self.coreVal
    @property
    def err(self):
        return np.abs(self.unc)
    @property
    def unc(self):
        return np.abs(self.unc)


class Unit:
    def __init__(self,name,system,kind,factor):
        self.name = name
        self.system = system
        self.kind = kind
        self.factor = factor
    def isAngle(self):
        return self.kind=='Angle'
    def isLength(self):
        return self.kind=='Length'
    def isEnglish(self):
        return self.system=='English'
    def isMetric(self):
        return self.system=='Metric'
    @property
    def system(self):
        return self.system
    @property
    def kind(self):
        return self.kind
    @property
    def factor(self):
        return self.factor
    def __str__(self):
        return self.name
    def __repr__(self):
        return str(self)

class MakeUnits:
    metricPrefixes = {'T':10**12,'G':10**9,'M':10**6,'k':10**3,'':1.0,'c':10**-2,'m':10**-3,'u':10**-6,\
    'n':10**-9,'p':10**-12,'f':10**-15,'a':10**-18,'z':10**-21}
    def __init__(self):
        self.units = {}
        self.kinds = []
        f = open(os.path.dirname(os.path.realpath(__file__)) +'/lib/units.txt','r')
        for line in f.readlines():
            l = line.split(',')
            # print l
            unit = l[0].strip()
            system = l[1].strip()
            kind = l[2].strip()
            val = float(l[3].strip())
            # print val
            if kind not in self.kinds:
                self.kinds.append(kind)
            if system == 'Metric':
                for pre in self.metricPrefixes:
                    self.units[pre+unit] = Unit(pre+unit,system,kind,val*self.metricPrefixes[pre])
            else:
                self.units[unit] = Unit(unit,system,kind,val)
        f.close()
    def __getattr__(self,unit):
        if unit in self.units.keys():
            return self.units[unit]
        else:
            raise AttributeError('Trying to convert to unknown unit!')
    def Get(self,unit):
        if unit in self.units.keys():
            return self.units[unit]
        else:
            raise AttributeError('Trying to convert to unknown unit!')
    @property
    def globalUnits(self):
        return self.units.values()
    @property
    def kinds(self):
        return self.kinds

GlobalUnits = MakeUnits()


class UnitHolder:
    def __init__(self,units=None):
        if not units:
            self.units = {}
        else:
            self.units = units
    def SetUnit(self,unit,pow):
        self.units[unit] = pow
    def AddToUnit(self,unit,pow):
        if unit in self.units.keys():
            self.units[unit] += pow
        else:
            self.units[unit] = pow
    def GetPow(self,unit):
        if not isinstance(unit,str):
            unit = str(unit)
        return self.units[unit]
    def GetUnit(self,unit):
        if unit in self.units:
            return GlobalUnits.Get(unit),self.units[unit]
        else:
            return None
    def GetLengthUnits(self):
        LU = []
        for unit in self.units:
            u = GlobalUnits.Get(unit)
            if u.isLength() or u.isEnglish():
                LU.append((u,self.GetPow(unit)))
        return LU
    def GetAngleUnits(self):
        AU = []
        for unit in self.units:
            u = GlobalUnits.Get(unit)
            if u.isAngle():
                AU.append((u,self.GetPow(unit)))
        return AU
    def ClearLengthUnits(self):
        for unit in self.units.keys():
            if GlobalUnits.Get(unit).isLength():
                self.units.pop(unit)
    def ClearAngleUnits(self):
        for unit in self.units.keys():
            if GlobalUnits.Get(unit).isAngle():
                self.units.pop(unit)
    def GetUnitsOf(self,kind):
        U = []
        for unit in self.units:
            u = GlobalUnits.Get(unit)
            if u.kind == kind:
                U.append((u,self.GetPow(unit)))
        return U
    def ClearUnitsOf(self,kind):
        for unit in self.units.keys():
            if GlobalUnits.Get(unit).kind == kind:
                self.units.pop(unit)
    def GetAllUnits(self):
        return [(GlobalUnits.Get(unit),self.units[unit]) for unit in self.units]
    def Reset(self):
        self.units = {}
    def Merge(self,UH):
        for unit,power in UH.GetAllUnits():
            self.AddToUnit(unit.name,power)
        return self
    def Inverse(self):
        d = copy.deepcopy(self.units)
        for i in d:
            d[i] *= -1
        return UnitHolder(d)
    def RaiseTo(self,pow):
        for i in self.units:
            self.units[i] *= pow
    def Copy(self):
        return copy.deepcopy(self)
    def GetLengthFactor(self):
        f = 1.0
        for unit,pow in self.GetLengthUnits():
            f *= unit.factor**pow
        return f
    def GetAngleFactor(self):
        f = 1.0
        for unit,pow in self.GetAngleUnits():
            f *= unit.factor**pow
        return f
    def GetFactorOf(self,kind):
        f = 1.0
        for unit,pow in self.GetUnitsOf(kind):
            f *= unit.factor**pow
        return f
    def GetTotalFactor(self):
        f = 1.0
        for unit,pow in self.GetAllUnits():
            f *= unit.factor**pow
        return f
    @property
    def dimensions(self):
        dim = {u:0 for u in GlobalUnits.kinds}
        for unit,power in self.GetAllUnits():
            dim[unit.kind] += power
        return dim

class UnitObj(CoreValWithErr):
    def __init__(self,coreVal,units):
        self.SetCoreVal(coreVal)
        if isinstance(units,UnitHolder):
            self.units = units.Copy()
        elif isinstance(units,list):
            self.units = UnitHolder()
            for u in units:
                self.units.AddToUnit(u[0],float(u[1]))
        elif isinstance(units,tuple):
            self.units = UnitHolder()
            for u in units:
                self.units.AddToUnit(units[0],float(units[1]))
        elif isinstance(units,str):
            self.units = UnitHolder()
            # sstr = str.split()
            ustr = self.ParseUnits(units)
            for s in ustr:
                self.units.AddToUnit(s[0],s[1])
        else:
            raise TypeError('Unexpected unit type!')
    def InUnitsOf(self,UO):
        assert self.dimensions == UO.dimensions
        thisFactor = self.units.GetTotalFactor()
        otherFactor = UO.units.GetTotalFactor()
        cv = self.coreVal*thisFactor/otherFactor
        return UnitObj(cv,UO.units)
    def SetCoreVal(self,coreVal):
        if isinstance(coreVal,CoreValWithErr):
            self.coreVal = coreVal
        elif isinstance(coreVal,Number):
            self.coreVal = CoreValWithErr(coreVal)
        elif isinstance(coreVal,tuple):
            self.coreVal = CoreValWithErr(coreVal[0],coreVal[1])
        else:
            raise TypeError('Unexpected coreVal type!')
    def ParseUnit(self,unit):
        if unit == '':
            return None
        ustr = unit.split('^')
        if len(ustr) == 1:
            ustr.append(1)
        else:
            ustr[1] = float(ustr[1])
        return ustr
    def ParseUnits(self,unitstr):
        unitSplit = unitstr.split()
        ustr = []
        for i in range(len(unitSplit)):
            unitSplit[i] = unitSplit[i].split('/')
        for unitSet in unitSplit:
            if len(unitSet) == 1:
                ustr.append(self.ParseUnit(unitSet[0]))
            else:
                for i in range(len(unitSet)):
                    u = self.ParseUnit(unitSet[i])
                    if u is None:
                        continue
                    if i > 0:
                        u[1] *= -1.0
                    ustr.append(u)
        return ustr
    # def CollectLength(self):
    #     lengthCheck = []
    #     power = self.dimensions['Length']
    #     oldFactor = self.units.GetLengthFactor()
    #     if power != 0:
    #         for unit in GlobalUnits.globalUnits:
    #             if unit.isMetric() and unit.isLength():
    #                 sortVal = (self.coreVal.val*oldFactor/(unit.factor**power))
    #                 absLogSV = np.abs(np.log10(sortVal))
    #                 lengthCheck.append((absLogSV,sortVal,unit))
    #         lengthCheck.sort()
    #         newUnit = lengthCheck[0][2]
    #         coreValFactor = newUnit.factor**power/oldFactor
    #     else:
    #         newUnit = None
    #         coreValFactor = 1/oldFactor
    #     self.units.ClearLengthUnits()
    #     if newUnit:
    #         self.units.SetUnit(newUnit.name,power)
    #     self.coreVal /= coreValFactor
    #     return self
    # def CollectAngle(self):
    #     angleCheck = []
    #     power = self.dimensions['Angle']
    #     oldFactor = self.units.GetAngleFactor()
    #     if power != 0:
    #         for unit in GlobalUnits.globalUnits:
    #             if unit.isAngle():
    #                 sortVal = (self.coreVal.val*oldFactor/(unit.factor**power))
    #                 absLogSV = np.abs(np.log10(sortVal))
    #                 angleCheck.append((absLogSV,sortVal,unit))
    #         angleCheck.sort()
    #         newUnit = angleCheck[0][2]
    #         coreValFactor = newUnit.factor**power/oldFactor
    #     else:
    #         newUnit = None
    #         coreValFactor = 1/oldFactor
    #     self.units.ClearAngleUnits()
    #     if newUnit:
    #         self.units.SetUnit(newUnit.name,power)
    #     self.coreVal /= coreValFactor
    #     return self
    def CollectMetricKind(self,kind):
        check = []
        power = self.dimensions[kind]
        oldFactor = self.units.GetFactorOf(kind)
        if power != 0:
            for unit in GlobalUnits.globalUnits:
                if unit.kind == kind and unit.system == 'Metric':
                    sortVal = (self.coreVal.val*oldFactor/(unit.factor**power))
                    absLogSV = np.abs(np.log10(sortVal))
                    check.append((absLogSV,sortVal,unit))
            check.sort()
            newUnit = check[0][2]
            coreValFactor = newUnit.factor**power/oldFactor
        else:
            newUnit = None
            coreValFactor = 1/oldFactor
        self.units.ClearUnitsOf(kind)
        if newUnit:
            self.units.SetUnit(newUnit.name,power)
        self.coreVal /= coreValFactor
        return self
    def CollectNonMetricKind(self,kind):
        check = []
        power = self.dimensions[kind]
        oldFactor = self.units.GetFactorOf(kind)
        if power != 0:
            # print self.units.GetUnitsOf(kind)[0][0]
            newUnit = self.units.GetUnitsOf(kind)[0][0]
            coreValFactor = newUnit.factor**power/oldFactor
        else:
            newUnit = None
            coreValFactor = 1/oldFactor
        self.units.ClearUnitsOf(kind)
        if newUnit:
            self.units.SetUnit(newUnit.name,power)
        self.coreVal /= coreValFactor
        return self
    def Collect(self):
        for kind in self.units.dimensions:
            if kind == 'Angle' or kind == 'Charge':
                self.CollectNonMetricKind(kind)
            else:
                self.CollectMetricKind(kind)
        return self
    def ConvertLength(self,newUnit):
        nU = GlobalUnits.Get(newUnit)
        if not nU.isLength():
            raise TypeError('Trying to convert to wrong type of unit!')

        if self.units.dimensions['Length']:
            hasLength = True
        else:
            hasLength = False

        if self.units.dimensions['Area']:
            hasArea = True
        else:
            hasArea = False

        if hasArea:
            self.CollectMetricKind('Area')
            oldUnit = self.units.GetUnitsOf('Area')
            if not len(oldUnit) == 1:
                raise ValueError('Quantity does not have the proper units for conversion!')
            oldpower = oldUnit[0][1]
            newpower = oldpower*2.0
            oldFactor = oldUnit[0][0].factor**oldpower
            newFactor = nU.factor**newpower
            self.units.ClearUnitsOf('Area')

        if hasLength:
            self.CollectMetricKind('Length')
            oldUnit = self.units.GetLengthUnits()
            newpower = oldUnit[0][1]
            oldFactor = oldUnit[0][0].factor**newpower
            newFactor = nU.factor**newpower
            self.units.ClearLengthUnits()

        self.coreVal*= oldFactor/newFactor
        self.units.SetUnit(newUnit,newpower)
    def ConvertAngle(self,newUnit):
        nU = GlobalUnits.Get(newUnit)
        if not nU.isAngle():
            raise TypeError('Trying to convert to wrong type of unit!')
        self.CollectNonMetricKind('Angle')
        oldUnit = self.units.GetAngleUnits()
        if not len(oldUnit) ==1:
            raise ValueError('Quantity does not have the proper units for conversion!')
        power = oldUnit[0][1]
        oldFactor = oldUnit[0][0].factor**power
        newFactor = nU.factor**power
        self.coreVal*= oldFactor/newFactor
        self.units.ClearAngleUnits()
        self.units.SetUnit(newUnit,power)
    def Convert(self,newUnit):
        nU = GlobalUnits.Get(newUnit)
        oldUnit = self.units.GetUnitsOf(nU.kind)
        if not len(oldUnit) == 1:
            raise ValueError('Quantity does not have the proper units for conversion!')
        power = oldUnit[0][1]
        oldFactor = oldUnit[0][0].factor**power
        newFactor = nU.factor**power
        self.coreVal*= oldFactor/newFactor
        self.units.ClearUnitsOf(nU.kind)
        self.units.SetUnit(newUnit,power)
    def ConvertArea(self,newUnit):
        nU = GlobalUnits.Get(newUnit)
        if self.units.dimensions['Length']:
            hasLength = True
        else:
            hasLength = False
        if self.units.dimensions['Area']:
            hasArea = True
        else:
            hasArea = False
        if hasArea:
            self.Convert(newUnit)
        if hasLength:
            self.CollectMetricKind('Length')
            oldUnit = self.units.GetUnitsOf('Length')
            if not len(oldUnit) == 1:
                raise ValueError('Quantity does not have the proper units for conversion!')
            oldpower = oldUnit[0][1]
            newpower = oldpower/2.0
            oldFactor = oldUnit[0][0].factor**oldpower
            newFactor = nU.factor**newpower
            self.units.ClearUnitsOf('Length')
            self.coreVal*= oldFactor/newFactor
            self.units.SetUnit(newUnit,newpower)
    def Copy(self):
        return copy.deepcopy(self)
    def __repr__(self):
        return str(self.coreVal) + ' ' + self.GetUnits()
    def __str__(self):
        return repr(self)
    def GetUnits(self):
        # nummap = {ord(c): ord(t) for c, t in zip(u"0123456789.", u"⁰¹²³⁴⁵⁶⁷⁸⁹ˑ")}
        unitStr = ''
        for unit,power in self.units.GetAllUnits():
            if power == 1:
                unitStr += '{} '.format(unit)
            elif power == 0:
                unitStr = ''
            else:
                unitStr += '{}^{} '.format(unit,power)
        # s = unicode(unitStr).translate(nummap)
        # return s.encode("utf-8")
        return unitStr
    def __add__(self,n):
        if isinstance(n,UnitObj):
            if not n.dimensions == self.dimensions:
                raise TypeError('Adding two values with different dimensions!')
            cv = self.coreVal + n.InUnitsOf(self).coreVal
            return UnitObj(cv,self.units)
        else:
            assert(isinstance(n,Number))
            cv = self.coreVal + n
            return UnitObj(cv,self.units)
    def __sub__(self,n):
        if isinstance(n,UnitObj):
            if not n.dimensions == self.dimensions:
                raise TypeError('Subtracting two values with different dimensions!')
            cv = self.coreVal - n.InUnitsOf(self).coreVal
            return UnitObj(cv,self.units)
        else:
            assert(isinstance(n,Number))
            cv = self.coreVal - n
            return UnitObj(cv,self.units)
    def __radd__(self, n):
        if isinstance(n,UnitObj):
            if not n.dimensions == self.dimensions:
                raise TypeError('Adding two values with different dimensions!')
            cv = self.coreVal + n.InUnitsOf(self).coreVal
            return UnitObj(cv,self.units)
        else:
            assert(isinstance(n,Number))
            cv = self.coreVal + n
            return UnitObj(cv,self.units)
    def __rsub__(self, n):
        if isinstance(n,UnitObj):
            if not n.dimensions == self.dimensions:
                raise TypeError('Subtracting two values with different dimensions!')
            cv = n.InUnitsOf(self).coreVal - self.coreVal
            return UnitObj(cv,self.units)
        else:
            assert(isinstance(n,Number))
            cv = n - self.coreVal
            return UnitObj(cv,self.units)
    def __mul__(self,n):
        if isinstance(n,Number):
            cv = self.coreVal * n
            return UnitObj(cv,self.units).Collect()
        elif isinstance(n,tuple) and len(n) is 2:
            val = ValWithErr(n[0],n[1])
            return self*val
        elif isinstance(n,UnitObj):
            cv = self.coreVal * n.coreVal
            nU = self.units.Copy()
            nU.Merge(n.units)
            return UnitObj(cv,nU).Collect()
        else:
            raise TypeError('Multiplying two unrelated objects!')
    def __rmul__(self,n):
        if isinstance(n,Number):
            cv = self.coreVal * n
            return UnitObj(cv,self.units).Collect()
        elif isinstance(n,tuple) and len(n) is 2:
            val = ValWithErr(n[0],n[1])
            return self*val
        elif isinstance(n,UnitObj):
            cv = self.coreVal * n.coreVal
            nU = self.units.Copy()
            nU.Merge(n.units)
            return UnitObj(cv,nU).Collect()
        else:
            raise TypeError('Multiplying two unrelated objects!')
    def __truediv__(self,n):
        if isinstance(n,Number):
            cv = self.coreVal / n
            return UnitObj(cv,self.units).Collect()
        elif isinstance(n,UnitObj):
            cv = self.coreVal / n.coreVal
            nU = self.units.Copy()
            nU.Merge(n.units.Inverse())
            return UnitObj(cv,nU).Collect()
        else:
            raise TypeError('Multiplying two unrelated objects!')
    def __div__(self,n):
        return self.__truediv__(n)
    def __rtruediv__(self, n):
        if isinstance(n,Number):
            cv = n / self.coreVal
            return UnitObj(cv,self.units.Inverse()).Collect()
        elif isinstance(n,UnitObj):
            cv = n.coreVal / self.coreVal
            nU = self.units.Inverse()
            nU.Merge(n.units)
            return UnitObj(cv,nU).Collect()
        else:
            raise TypeError('Multiplying two unrelated objects!')
    def __rdiv__(self, n):
        return self.__rtruediv__(n)
    def __pow__(self,pow):
        assert isinstance(pow,Number)
        cv = self.coreVal**pow
        nU = self.units.Copy()
        nU.RaiseTo(pow)
        return UnitObj(cv,nU)
    def __neg__(self):
        cv = -self.coreVal
        return UnitObj(cv,self.units)
    @property
    def val(self):
        return self.coreVal.val
    @property
    def err(self):
        return self.coreVal.err
    @property
    def unc(self):
        return self.err
    @property
    def units(self):
        return self.units
    @property
    def coreVal(self):
        return self.coreVal
    def ValAs(self,unit):
        return self.As(unit).coreVal
    def As(self,unit):
        u = self.Copy()
        if GlobalUnits.Get(unit).isLength():
            u.ConvertLength(unit)
        elif GlobalUnits.Get(unit).kind == 'Area':
            u.ConvertArea(unit)
        else:
            u.Convert(unit)
        return u
    def __getattr__(self,unit):
        if unit in GlobalUnits.units:
            return self.As(unit)
        else:
            raise AttributeError('Trying to convert to unknown unit!')
    @property
    def dimensions(self):
        return self.units.dimensions

class AngleUnit(UnitObj):
    default = 'deg'
    def __init__(self,coreVal,unit=None,power=1):
        if unit is None:
            unit = self.default
        self.SetCoreVal(coreVal)
        self.units = UnitHolder()
        self.units.SetUnit(unit,power)
    @classmethod
    def SetDefault(cls,unit):
        setattr(cls,"default",unit)
    @classmethod
    def SetDegrees(cls):
        setattr(cls, "default", "deg")
    @classmethod
    def SetRadians(cls):
        setattr(cls, "default", "rad")
    @classmethod
    def SetGon(cls):
        setattr(cls, "default", "gon")

class LengthUnit(UnitObj):
    default = 'cm'
    def __init__(self,coreVal,unit=None,power=1):
        if unit is None:
            unit = self.default
        self.SetCoreVal(coreVal)
        self.units = UnitHolder()
        self.units.SetUnit(unit,power)
    @classmethod
    def SetDefault(cls,unit):
        setattr(cls,"default",unit)

class ValWithErr(UnitObj):
    def __init__(self,val,unc):
        self.SetCoreVal((val,unc))
        self.units = UnitHolder()

class GetUnits:
    def __getattr__(self,u):
        return UnitObj(1,u)
    def read(self,unitstr):
        return UnitObj(1,unitstr)


class CompUnits:
    def __init__(self):
        u = GetUnits()
        self.comp = {}
        self.comp['A'] =  (u.A)/(u.C/u.s)
        self.comp['b'] = (u.b)/(1.0e-28 * u.m**2)
        self.comp['g'] = (1.782662e-33*u.g)/(u.eV)
    def __getattr__(self,u):
        if u in self.comp:
            return self.comp[u]
        else:
            raise AttributeError('Composite unit not defined')

class Constants:
    def __init__(self):
        u = GetUnits()
        self.constants = {}
        self.constants['hbar'] = (6.582119514e-16 * u.eV * u.s)
        self.constants['c'] = (299792458.0) * u.m/u.s
    def __getattr__(self,u):
        if u in self.constants:
            return self.constants[u]
        else:
            raise AttributeError('Composite unit not defined')

unit = GetUnits()
gen = CompUnits()
consts = Constants()
