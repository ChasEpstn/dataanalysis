from __future__ import division
from BTrees.OOBTree import OOBTree as BTree
from BTrees.IOBTree import IOBTree as IBTree
import zc.zlibstorage
import ZODB.FileStorage, ZODB, ZODB._compat
import transaction
from .Histograms import *
import subprocess as sp
import itertools
import os
import logging

logging.getLogger('ZODB.FileStorage').addHandler(logging.NullHandler())

if ZODB._compat._protocol == 1:
    print 'Need to patch ZODB protocol.\n Please enter sudo password'
    sp.call(['sudo','python', os.path.dirname(os.path.realpath(__file__))+'/patchZODB.py',ZODB._compat.__file__])
    if ZODB._compat._protocol == 2:
        print 'ZODB successfully patched. Rerun program as usual.'
        quit()
    else:
        print 'Trouble patching ZODB. Try something else...'
        quit()

class QFile:
    def __init__(self,fileName,flag='r'):
        if not fileName.endswith('.qs'):
            fileName = fileName + '.qs'
        if flag == 'w':
            if os.path.exists(fileName):
                os.remove(fileName)
            print 'Creating new file',fileName
        elif flag == 'r':
            print 'Opening existing file',fileName
        elif flag == 'e':
            print 'Editing existing file',fileName
        else:
            raise ValueError('Incorrect open flag for QFile!')
        self.fileName = fileName
        self.storage = zc.zlibstorage.ZlibStorage(
            ZODB.FileStorage.FileStorage(self.fileName))
        self.db = ZODB.DB(self.storage)
        self.connection = self.db.open()
        self.root = self.connection.root
        self.stores = False
        self.trees = False
        self.open = True
        self.flag = flag
    def InitStore(self):
        self.root.stores = BTree()
        self.stores = True
    def InitTree(self):
        self.root.trees = BTree()
        self.trees = True
    def NewTree(self,treeName):
        if not self.trees:
            self.InitTree()
        self.root.trees[treeName] = QTree()
        return self.root.trees[treeName]
    def NewStore(self,storeName):
        if not self.stores:
            self.InitStore()
        self.root.stores[storeName] = QStore()
        return self.root.stores[storeName]
    def AddTree(self,tree,treeName):
        if not self.trees:
            self.InitTree()
        self.root.trees[treeName] = tree
    def AddStore(self,store,storeName):
        if not self.stores:
            self.InitStore()
        self.root.stores[storeName] = store
    def GetTree(self,treeName):
        return self.root.trees[treeName]
    def GetStore(self,storeName):
        return self.root.stores[storeName]
    def ListTrees(self):
        return [str(i) for i in self.root.trees]
    def ListStores(self):
        return [str(i) for i in self.root.stores]
    def Close(self):
        print 'Closing File'
        if self.flag == 'w' or self.flag == 'e':
            transaction.commit()
            self.db.pack()
            ext = ['.index','.lock','.tmp']
            for f in [self.fileName+e for e in ext]:
                if os.path.exists(f):
                    os.remove(f)
        self.open = False
    def __del__(self):
        if self.open:
            self.Close()

class QTree:
    def __init__(self):
        self.tree = IBTree()
        self.nCalls = 0
        self.branches = None
    def Add(self,obj):
        self.tree[self.nCalls] = obj
        self.nCalls += 1
        if self.nCalls % 10000 == 0:
            transaction.savepoint(True)
    def Read(self):
        return self.tree.values()
    def Get(self,val):
        return self.tree[val]
    def __iter__(self):
        return self.tree.itervalues()
    def AddBranch(self,branchName):
        if not self.branches:
            self.branches = BTree()
        self.branches[branchName] = QTree()
        return self.branches[branchName]
    def GetBranch(self,branchName):
        return self.branches[branchName]
    def ListBranches(self):
        return [str(i) for i in self.branches]
    def __getstate__(self):
        state = (self.tree,self.branches,self.nCalls,)
        return state
    def __setstate__(self, state):
        tree,branches,nCalls = state
        self.tree = tree
        self.branches = branches
        self.nCalls = nCalls

class QStore:
    def __init__(self):
        self.store = BTree()
    def Add(self,obj,tag):
        self.store[tag] = obj
    def Get(self,tag):
        return self.store[tag]
    def __iter__(self):
        return self.store.itervalues()
    def Tags(self):
        return [str(i) for i in self.store]
    def Read(self):
        return self.store.values()
    def __getstate__(self):
        state = self.store
        return state
    def __setstate__(self, state):
        self.store = state
