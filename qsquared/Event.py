from .Vectors import *

class ParticleVector(FourVector):
    def __init__(self,hepID,q):
        self.hepID = hepID
        if isinstance(q,FourVector):
            q = q.values
            self.q = np.array(q)
        else:
            assert(len(q) is 4)
            self.q = np.array(q)

class Event:
    def __init__(self):
        self.weight = None
        self.id = None
        self.particles = ()
    # def AddParticleVector(self,part):
    #     self.particles.append(part)
    def AddParticleIDEPxPyPz(self,hepID,e,px,py,pz):
        # self.particles.append([hepID,e,px,py,pz])
        self.particles += ((hepID,e,px,py,pz),)
    def GetParticle(self,p):
        return ParticleVector(self.particles[p])
    def GetParticles(self):
        return [ParticleVector(p[0],p[1:5]) for p in self.particles]
    def SetWeight(self,weight):
        self.weight = weight
    def GetWeight(self):
        return self.weight
    def SetID(self,id):
        self.id = id
    @property
    def nPart(self):
        return len(self.particles)
    # def Done(self):
    #     pass
        # self.particles = tuple(self.particles)
