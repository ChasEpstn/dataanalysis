# -*- coding: utf-8 -*-
from __future__ import division
from math import *
from .Vectors import *
from .Units import *
import scipy.optimize
import os
import fractions

class Particle:
    def __init__(self,id, name,mass,width,charge):
        self.states = {}
        self.AddState(id,name,mass,width,charge)
    def __getattr__(self,attr):
        if attr in ['id','name','mass','width','charge']:
            if len(self.states.keys()) == 1:
                return self.states.values()[0][attr]
            else:
                raise AttributeError('Need to choose a charge state.')
        elif attr in ['minus','zero','plus','plusplus']:
            return Particle(*self.states[attr].values())
        else:
            raise AttributeError('Particle attribute does not exist!')
    def AddState(self,id,name,mass,width,charge):
        attrs = {}
        attrs['id'] = id
        attrs['name'] = name
        attrs['mass'] = mass
        attrs['width'] = width
        attrs['charge'] = charge
        if charge == -1:
            sc = 'minus'
        elif charge == 0:
            sc = 'zero'
        elif charge == 1:
            sc = 'plus'
        elif charge == 2:
            sc = 'plusplus'
        else:
            sc = str(charge)
        self.states[sc] = attrs
    def __repr__(self):
        return 'Particle {}: mass {}'.format(self.name,self.mass)
    def __str__(self):
        return repr(self)
### For below
# *       1 -  8 \ Monte Carlo particle numbers as described in the "Review of
# *       9 - 16 | Particle Physics". Charge states appear, as appropriate,
# *      17 - 24 | from left-to-right in the order -, 0, +, ++.
# *      25 - 32 /
# *           33   blank
# *      34 - 51   central value of the mass (double precision)
# *           52   blank
# *      53 - 60   positive error
# *           61   blank
# *      62 - 69   negative error
# *           70   blank
# *      71 - 88   central value of the width (double precision)
# *           89   blank
# *      90 - 97   positive error
# *           98   blank
# *      99 -106   negative error
# *          107   blank
# *     108 -128   particle name left-justified in the field and
# *                charge states right-justified in the field.
# *                This field is for ease of visual examination of the file and
# *                should not be taken as a standardized presentation of
# *                particle names.

class Particles:
    def __init__(self):
        self.particles = {}
        f = open(os.path.dirname(os.path.realpath(__file__)) +'/lib/particles.txt','r')
        for line in f.readlines():
            if line.startswith('*'): continue
            id1 = line[0:8].strip()
            id2 = line[8:16].strip()
            id3 = line[16:24].strip()
            id4 = line[24:32].strip()
            mass = float(line[33:51])
            merrp = float(line[52:60])
            merrm = float(line[61:69])
            w = line[70:88].strip()
            if w != '':
                width = float(w)
                werrp = float(line[89:97])
                werrm = float(line[98:106])
            else:
                width = 0
                werrp = 0
                werrm = 0
            nameandcharges = line[107:128]

            ids = [int(x) for x in id1,id2,id3,id4 if x != '']
            merr = max([np.abs(merrp),np.abs(merrm)])
            werr = max([np.abs(werrp),np.abs(werrm)])
            nc = nameandcharges.split(' ')
            name = self.ParseName(nc[0])
            scharges = nc[-1].split(',')
            ncharges = []
            for charge in scharges:
                try:
                    c =float(fractions.Fraction(x))
                except(ValueError):
                    c = -1*charge.count('-') + charge.count('+')
                ncharges.append(c)
            if len(ids) != len(ncharges):
                print 'Bad parse of pdg file'
                continue
            for i in range(len(ids)):
                if name not in self.particles:
                    self.particles[name] = Particle(ids[i], name,UnitObj((mass,merr),'GeV'),UnitObj((width,werr),'GeV'),ncharges[i])
                else:
                    self.particles[name].AddState(ids[i], name,UnitObj((mass,merr),'GeV'),UnitObj((width,werr),'GeV'),ncharges[i])
        f.close()
    def GetParticle(self,name):
        return self.particles[name]
    def __getattr__(self,name):
        if name in self.particles:
            return self.GetParticle(name)
        else:
            raise AttributeError('Particle not in database!')
    def ParseName(self,name):
        name = name.lower()
        name = name.replace('/','')
        name= name.replace('(1s)','')
        name = name.replace('*','_star')
        name = name.replace('(','_')
        name = name.replace(')','')
        name = name.replace("'","_prime")
        return name

# class Masses:
#     def __init__(self, unit='MeV'):
#         self.masses = {}
#         if unit == 'MeV':
#             self.f = 1.0e3
#         elif unit == 'GeV':
#             self.f = 1.0
#         elif unit == 'keV':
#             self.f = 1.0e6
#         else:
#             print 'Invalid unit choice. Using MeV.'
#             self.f = 1.0e3
#         f = open(os.path.dirname(os.path.realpath(__file__)) +'/lib/particles.txt','r')
#         for line in f.readlines():
#             if line.startswith('*'): continue
#             l = line.split()
#             name = l[-2]
#             mass = float(l[1])
#             self.masses[name] = mass
#     def __getattr__(self,name):
#         if name in self.masses:
#             return self.f*self.masses[name]
#         else:
#             raise AttributeError('Particle not in database!')
#     @property
#     def electron(self):
#         return self.e
#     @property
#     def muon(self):
#         return self.mu
#     @property
#     def proton(self):
#         return self.p
#     @property
#     def neutron(self):
#         return self.n
#     @property
#     def carbon(self):
#         return self.f*11.1780
#     def Mass(self,particle):
#         return self.masses[particle]

class TwoBody:
    def __init__(self,p1,p2):
        self.p1 = p1
        self.p2 = p2
        self.p3 = None
        self.p4 = None
    def InLab(self):
        if self.p2.p == 0:
            return self.p1,self.p2
        p1 = self.p1.InFrameOf(self.p2)
        p2 = self.p2.InFrameOf(self.p2)
        return p1,p2
    def InCM(self):
        tot = self.p1 + self.p2
        if tot.p == 0:
            return self.p1,self.p2
        p1 = self.p1.InFrameOf(tot)
        p2 = self.p2.InFrameOf(tot)
        return p1,p2
    def OutLab(self):
        if self.p2.p == 0:
            return self.p3,self.p3
        p3 = self.p3.InFrameOf(self.p2)
        p4 = self.p4.InFrameOf(self.p2)
        return p3,p4
    def OutCM(self):
        tot = self.p1 + self.p2
        if tot.p == 0:
            return self.p3,self.p4
        p3 = self.p3.InFrameOf(tot)
        p4 = self.p4.InFrameOf(tot)
        return p3,p4
    def SetLab(self):
        if self.p3 is not None and self.p4 is not None:
            self.p3,self.p4 = self.OutLab()
        self.p1,self.p2 = self.InLab()
    def SetCM(self):
        if self.p3 is not None and self.p4 is not None:
            self.p3,self.p4 = self.OutCM()
        self.p1,self.p2 = self.InCM()
    def CalcP3(self,theta,phi=0):
        self.dir = ThreeVector([sin(theta)*cos(phi),sin(theta)*sin(phi),cos(theta)])
        self.msolve = self.p1.m
        self.mfree = self.p2.m
        p3s = scipy.optimize.brentq(self.ECons,1e-6,self.p1.p)
        E3 = sqrt(p3s**2 + self.msolve**2)
        p3 = self.dir*p3s
        self.p3 = FourVector([E3]+list(p3.values))
        self.p4 = self.p1 + self.p2 - self.p3
        return self.p3
    def CalcP4(self,theta,phi=0):
        self.dir = ThreeVector([sin(theta)*cos(phi),sin(theta)*sin(phi),cos(theta)])
        self.msolve = self.p2.m
        self.mfree = self.p1.m
        p4s = scipy.optimize.brentq(self.ECons,1e-6,self.p2.p)
        E4 = sqrt(p4s**2 + self.msolve*2)
        p4 = self.dir*p4s
        self.p4 = FourVector([E4]+list(p4.values))
        self.p3 = self.p1 + self.p2 - self.p4
        return self.p4
    def RemainingP(self,p):
        pp1 = self.p1.threeVector
        pp2 = self.p2.threeVector
        pp3 = p.threeVector
        pp4 = pp1 + pp2 - pp3
        E4 = sqrt(pp4**2.0 + self.mfree**2)
        return FourVector([E4,pp4.x,pp4.y,pp4.z])
    def ECons(self,p):#arg: momentum of particle corresponding to self.dir & self.msolve
        m = self.msolve
        E3 = sqrt(p**2 + m**2)
        P3 = FourVector([E3,p*self.dir.x,p*self.dir.y,p*self.dir.z])
        P4 = self.RemainingP(P3)
        return self.p1.e + self.p2.e - P3.e - P4.e


class BeamKinematics:
    def __init__(self,m):
        self.m = m
        self.dir = ThreeVector([0,0,1])
        print 'Constructing beam with particle mass {} in Z direction'.format(self.m)
    def E(self,E):
        self.E = E
        self.T = E - self.m
        self.P = sqrt(self.E**2 - self.m**2)
        return self.Get4Vector()
    def T(self,T):
        self.T = T
        self.E = T + self.m
        self.P = sqrt(self.E**2 - self.m**2)
        return self.Get4Vector()
    def P(self,P):
        self.P = P
        self.E = sqrt(self.P**2 + self.m**2)
        self.T = self.E - self.m
        return self.Get4Vector()
    def SetDir(self,dir):
        self.dir = dir
        return self
    def Get4Vector(self):
        return FourVector([self.E]+list(self.P*self.dir.values))

particle = Particles()
