from __future__ import division
from numbers import Number

import numpy as np
cimport numpy as np
from .Units import sqrt

cdef class ThreeVector:
    # cdef public np.ndarray q
    def __init__(self,q=[0,0,0]):
        assert(len(q) is 3)
        self.q = np.array(q)
    def __add__(self,v):
        assert(isinstance(v,ThreeVector))
        return ThreeVector(self.q+v.q)
    def __mul__(self,v):
        a = isinstance(self,ThreeVector)
        b = isinstance(v,ThreeVector)
        # if isinstance(v,ThreeVector):
        #     return np.sum(self.q*v.q)
        # elif isinstance(v,Number):
        #     return ThreeVector(v*self.q)
        if a and b:
            return np.sum(self.q*v.q)
        elif a or b:
            if a:
                return ThreeVector(v*self.q)
            elif b:
                return ThreeVector(self*v.q)
        else:
            raise TypeError('Must multiply by a number or another vector!')
    def __rmul__(self,v):
        # print 'RMUL: Self is {}, v is {}'.format(self,v)
        # if isinstance(v,ThreeVector):
        #     return np.sum(self.q*v.q)
        # elif isinstance(v,Number):
        #     return ThreeVector(v*self.q)
        # else:
        #     raise TypeError('Must multiply by a number or another vector!')
        a = isinstance(self,ThreeVector)
        b = isinstance(v,ThreeVector)
        if a and b:
            return np.sum(self.q*v.q)
        elif a or b:
            if a:
                return ThreeVector(v*self.q)
            elif b:
                return ThreeVector(self*v.q)
        else:
            raise TypeError('Must multiply by a number or another vector!')

    def __sub__(self,v):
        assert(isinstance(v,ThreeVector))
        return ThreeVector(self.q - v.q)
    def __truediv__(self,v):
        assert(isinstance(v,Number))
        return ThreeVector(self.q/v)
    def __pow__(self,pow,a):
        assert(pow==2)
        return np.sum(self.q**2)
    def __neg__(self):
        return ThreeVector(-self.q)
    def __repr__(self):
        return str(self.q)
    def PxPyPz(self,px,py,pz):
        self.q[0] = px
        self.q[1] = py
        self.q[2] = pz
        return self
    def XYZ(self,x,y,z):
        self.q[0] = x
        self.q[1] = y
        self.q[2] = z
        return self
    def RotateX(self,angle):
        theta = np.radians(angle)
        c = np.cos(theta)
        s = np.sin(theta)
        r = np.matrix('1 0 0; 0 {} {}; 0 {} {}'.format(c,-s,s,c))
        self.q = np.asarray(self.q*r).squeeze()
    def RotateY(self,angle):
        theta = np.radians(angle)
        c = np.cos(theta)
        s = np.sin(theta)
        r = np.matrix('{} 0 {}; 0 1 0; {} 0 {}'.format(c,s,-s,c))
        self.q = np.asarray(self.q*r).squeeze()
    def RotateZ(self,angle):
        theta = np.radians(angle)
        c = np.cos(theta)
        s = np.sin(theta)
        r = np.matrix('{} {} 0; {} {} 0; 0 0 1'.format(c,-s,s,c))
        self.q = np.asarray(self.q*r).squeeze()
    @property
    def x(self):
        return self.q[0]
    @property
    def y(self):
        return self.q[1]
    @property
    def z(self):
        return self.q[2]
    @property
    def phi(self):
        ph = np.arctan2(self.q[1],self.q[0])
        if ph < 0:
            ph += 2*np.pi
        return ph
    @property
    def pt(self):
        return sqrt(self.q[0]**2 + self.q[1]**2)
    @property
    def theta(self):
        return np.arctan2(self.pt,self.q[2])
    @property
    def values(self):
        return self.q
    @property
    def mag(self):
        return self.magnitude()
    cpdef double magnitude(self):
        return sqrt(self.q[0]**2  + self.q[1]**2 + self.q[2]**2)
    @property
    def unitVector(self):
        return ThreeVector(self.values/self.mag)


cdef class FourVector:
    # cdef public np.ndarray q
    def __init__(self,q=[0,0,0,0]):
        assert(len(q) is 4)
        self.q = np.array(q)
    def EPxPyPz(self,e,px,py,pz):
        self.q[0] = e
        self.q[1] = px
        self.q[2] = py
        self.q[3] = pz
        return self
    def XYZM(self,x,y,z,m):
        self.q[1] = x
        self.q[2] = y
        self.q[3] = z
        self.q[0] = sqrt(x**2 + y**2 + z**2 + m**2)
        return self
    def __add__(self,v):
        assert(isinstance(v,FourVector))
        return FourVector(self.q+v.q)
    def __mul__(self,v):
        # print 'MUL: Self is {}, v is {}'.format(self,v)
        a = isinstance(self,FourVector)
        b = isinstance(v,FourVector)
        # if isinstance(v,ThreeVector):
        #     return np.sum(self.q*v.q)
        # elif isinstance(v,Number):
        #     return ThreeVector(v*self.q)
        if a and b:
            return self.q[0]*v.q[0] - self.q[1]*v.q[1] - self.q[2]*v.q[2] - self.q[3]*v.q[3]
        elif a or b:
            if a:
                return FourVector(v*self.q)
            elif b:
                return FourVector(self*v.q)
        else:
            raise TypeError('Must multiply by a number or another vector!')

        # if isinstance(v,FourVector):
        #     return self.q[0]*v.q[0] - self.q[1]*v.q[1] - self.q[2]*v.q[2] - self.q[3]*v.q[3]
        # elif isinstance(v,Number):
        #     return FourVector(v*self.q)
        # else:
        #     raise TypeError('Must multiply by a number or another vector!')
    def __rmul__(self,v):
        # print 'RMUL: Self is {}, v is {}'.format(self,v)
        # if isinstance(v,FourVector):
        #     return self.q[0]*v.q[0] - self.q[1]*v.q[1] - self.q[2]*v.q[2] - self.q[3]*v.q[3]
        # elif isinstance(v,Number):
        #     return FourVector(v*self.q)
        # else:
        #     raise TypeError('Must multiply by a number or another vector!')
        a = isinstance(self,FourVector)
        b = isinstance(v,FourVector)
        # if isinstance(v,ThreeVector):
        #     return np.sum(self.q*v.q)
        # elif isinstance(v,Number):
        #     return ThreeVector(v*self.q)
        if a and b:
            return self.q[0]*v.q[0] - self.q[1]*v.q[1] - self.q[2]*v.q[2] - self.q[3]*v.q[3]
        elif a or b:
            if a:
                return FourVector(v*self.q)
            elif b:
                return FourVector(self*v.q)
        else:
            raise TypeError('Must multiply by a number or another vector!')
    def __sub__(self,v):
        assert(isinstance(v,FourVector))
        return FourVector(self.q - v.q)
    def __truediv__(self,v):
        assert(isinstance(v,Number))
        return FourVector(self.q/v)
    def __pow__(self,pow,a):
        if isinstance(pow,FourVector):
            return self.InFrameOf(pow)
        else:
            assert(pow==2)
            return self.q[0]**2 - self.q[1]**2 - self.q[2]**2 - self.q[3]**2
    def __neg__(self):
        return FourVector([self.q[0],-self.q[1],-self.q[2],-self.q[3]])
    def __repr__(self):
        return str(self.q)
    def InFrameOf(self, vec):
        if not vec.beta == 0:
            g = vec.gamma
            bx = vec.betaX
            by = vec.betaY
            bz = vec.betaZ
            b = vec.beta
            Lambda = np.matrix('{} {} {} {}; {} {} {} {}; {} {} {} {}; {} {} {} {}'.format(
                g, -bx*g, -by*g, -bz*g,
                -bx*g, 1+(g-1)*bx**2/b**2, (g-1)*bx*by/b**2, (g-1)*bx*bz/b**2,
                -by*g, (g-1)*by*bx/b**2, 1+(g-1)*by**2/b**2, (g-1)*by*bz/b**2,
                -bz*g, (g-1)*bz*bx/b**2, (g-1)*bz*by/b**2, 1 + (g-1)*bz**2/b**2
            ))
            q = np.asarray(self.q*Lambda).squeeze()
        else:
            q = self.q
        return FourVector(q)
        # r = np.matrix('1 0 0; 0 {} {}; 0 {} {}'.format(c,-s,s,c))
        # self.q = np.asarray(self.q*r).squeeze()

    @property
    def threeVals(self):
        return self.q[1:4]
    @property
    def threeVector(self):
        return ThreeVector(self.q[1:4])
    @property
    def e(self):
        return self.q[0]
    @property
    def px(self):
        return self.q[1]
    @property
    def py(self):
        return self.q[2]
    @property
    def pz(self):
        return self.q[3]
    @property
    def E(self):
        return self.q[0]
    @property
    def Px(self):
        return self.q[1]
    @property
    def Py(self):
        return self.q[2]
    @property
    def Pz(self):
        return self.q[3]
    @property
    def x(self):
        return self.q[1]
    @property
    def y(self):
        return self.q[2]
    @property
    def z(self):
        return self.q[3]
    @property
    def m2(self):
        return self.q[0]**2 - self.q[1]**2 - self.q[2]**2 - self.q[3]**2
    @property
    def m(self):
        return sqrt(self.m2)
    @property
    def phi(self):
        ph = np.arctan2(self.q[2],self.q[1])
        if ph < 0:
            ph += 2*np.pi
        return ph
    @property
    def pt(self):
        return sqrt(self.q[1]**2 + self.q[2]**2)
    @property
    def Pt(self):
        return self.pt
    @property
    def theta(self):
        return np.arctan2(self.pt,self.q[3])
    @property
    def values(self):
        return self.q
    @property
    def p(self):
        return sqrt(self.q[1]**2 + self.q[2]**2 + self.q[3]**2)
    @property
    def P(self):
        return self.p
    @property
    def unitVector(self):
        return ThreeVector(self.threeVals/self.magnitude)
    @property
    def gamma(self):
        return self.e/self.m
    @property
    def beta(self):
        return self.p/self.e
    @property
    def betaX(self):
        return self.px/self.e
    @property
    def betaY(self):
        return self.py/self.e
    @property
    def betaZ(self):
        return self.pz/self.e



# a = FourVector([5,1,1,1])#[5,1,1,1])
# # print a.q
# # print a.m
# # print a.theta
# # print a.phi
# print a.unitVector().df()
#
# c = ThreeVector([1,2,3])
# print c.magnitude
# print c.unitVector().df()
# # b = pd.Series()
# # b.loc[0] = np.array(range(4))
# b = a.df()
# print b
