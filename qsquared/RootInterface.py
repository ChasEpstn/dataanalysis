from __future__ import division
import numpy as np
from .Histograms import *
from .G4Event import *
from .Event import *

try:
    import ROOT as r
except(ImportError):
    print 'Could not import ROOT! Things might not work properly'

class RootInterface:
    def __init__ (self,fileName,flag):
        r.gROOT.Reset()
        self.runDarkLight_Init()
        self.f = r.TFile(fileName,flag)
    def ls(self):
        self.f.ls()
    def readTree(self,treeName):
        t = self.f.Get(treeName)
        return t
    def readHist(self,histName):
        h = self.f.Get(histName)
        if 'TH1' in str(type(h)):
            bc = []
            bv = []
            be = []
            for i in range(h.GetNbinsX()):
                bc.append(h.GetBinCenter(i+1))
                bv.append(h.GetBinContent(i+1))
                be.append(h.GetBinError(i+1)**2.0)
            hh = H1(1,1,1)
            title = h.GetTitle()
            xlabel = h.GetXaxis().GetTitle()
            ylabel = h.GetYaxis().GetTitle()
            hh.SetFromData(np.array(bc),np.array(bv),np.array(be),np.zeros(4,dtype=np.float64),title,xlabel,ylabel)
            # hh.SetTitle(title)
            # hh.SetXLabel(xlabel)
            # hh.SetYLabel(ylabel)
            return hh
        elif 'TH2' in str(type(h)):
            bv = []
            be = []
            nx = h.GetNbinsX()
            ny = h.GetNbinsY()
            bcx = np.linspace(h.GetXaxis().GetBinCenter(1),
                h.GetXaxis().GetBinCenter(nx),nx)
            bcy = np.linspace(h.GetYaxis().GetBinCenter(1),
                h.GetYaxis().GetBinCenter(ny),ny)
            for i in range(nx):
                for j in range(ny):
                    bv.append(h.GetBinContent(i+1,j+1))
                    be.append(h.GetBinError(i+1,j+1)**2.0)
            bv = np.array(bv).reshape(nx,ny)
            be = np.array(be).reshape(nx,ny)
            title = h.GetTitle()
            xlabel = h.GetXaxis().GetTitle()
            ylabel = h.GetYaxis().GetTitle()
            hh = H2(1,1,1,1,1,1)
            hh.SetFromData(bcx,bcy,bv,be,np.zeros(6,dtype=np.float64),title,xlabel,ylabel)
            # hh.SetTitle(title)
            # hh.SetXLabel(xlabel)
            # hh.SetYLabel(ylabel)
            return hh
        else:
            raise StandardError('Someone fucked up')
    def convertHists(self,QF):
        namesAndTypes = [(a.GetName(),a.GetClassName()) for a in self.f.GetListOfKeys()]
        s = QF.NewStore('Histograms')
        for name,type in namesAndTypes:
            if 'TH1' in type or 'TH2' in type:
                print 'Saving hist {}...'.format(name)
                hist = self.readHist(name)
                s.Add(hist,name)
        return s
    def reDrawHists(self):
        namesAndTypes = [(a.GetName(),a.GetClassName()) for a in self.f.GetListOfKeys()]
        for name,type in namesAndTypes:
            if 'TH1' in type or 'TH2' in type:
                print 'Redrawing hist {}...'.format(name)
                hist = self.readHist(name)
                hist.Draw(name)
    def runDarkLight_Init(self):
        dir = '{}/lib/'.format(os.path.dirname(os.path.realpath(__file__)))
        r.gSystem.Load(dir+'libDLg4Event.dylib')
        r.gSystem.Load(dir+'libDLmadEvent.dylib')
    def getTrkNumHits(self,track):
    	num =0
    	try:
    		num = track.GetNumHits()
    	except ReferenceError:
    		pass
    	except AttributeError:
    		pass
    	return num
    def getEveNumTrks(self,event):
    	num = 0
    	try:
    		num = event.GetNumTracks()
    	except ReferenceError:
    		pass
    	except AttributeError:
    		pass
    	return num
    def convertRootTree(self,QF):
        DLg4 = self.readTree('DaLiEventTree')
        eve = QF.NewTree('DLg4Event')
        mad = QF.NewTree('DLmadEvent')
        counter = 0
        maxEve = 1000000000
        for event in DLg4:
            counter = counter + 1
            if counter>=maxEve:
                break
            if counter % 10000 == 0:
                print 'At event number',counter
            G4 = G4Event()
            # print event.eventID
            G4.SetEventID(event.DLg4Event.GetEventID())
            G4.SetWeight(event.DLg4Event.GetWeight())
            G4.SetGeneratorName(event.generatorName.Data())
            G4.SetBaseGeometry(event.baseGeometry.Data())


            MD = Event()
            MD.SetWeight(event.DLmadEvent.weight)
            MD.SetID(event.DLmadEvent.id)

            #Create DLmadEvent
            for i in range(event.DLmadEvent.nOutPart()):
                # if counter2>=maxEve:
                #     break
                # counter2 += 1
                # print counter2
                e = event.DLmadEvent.getOutParticle(i)
                MD.AddParticleIDEPxPyPz(e.hepId,e.P4.E(),e.P4.Px(),e.P4.Py(),e.P4.Pz())

            #Create DLg4Event
            numTrks = self.getEveNumTrks(event.DLg4Event)
            for j in range(numTrks):
                TRK = G4Track()
                track= event.DLg4Event.GetTrack(j)
                numHits = self.getTrkNumHits(track)
                TRK.SetTrackID(track.GetTrackID())
                TRK.SetParticleName(track.GetParticleName().Data())
                TRK.SetParentID(track.GetParentID())
                trkOrigin = track.GetOrigin()
                TRK.SetOrigin([trkOrigin.X(),trkOrigin.Y(),trkOrigin.Z()])
                trk4Mom = track.Get4Momentum()
                TRK.Set4Momentum([trk4Mom.E(),trk4Mom.Px(),trk4Mom.Py(),trk4Mom.Pz()])
                for k in range(numHits):
                    HIT = G4Hit()
                    hit = track.GetHit(k)
                    detID = hit.GetDetectorID().Data()
                    HIT.SetDetectorID(detID)
                    hitPos = hit.GetPosition()
                    HIT.SetPosition([hitPos.X(),hitPos.Y(),hitPos.Z()])
                    localPos = hit.GetLocalPosition()
                    HIT.SetLocalPosition([localPos.X(),localPos.Y(),localPos.Z()])
                    threeMom = hit.GetLocalMomentum()
                    HIT.Set3Momentum([threeMom.Px(),threeMom.Py(),threeMom.Pz()])
                    hitDE = hit.GetDE()
                    HIT.SetDE(hitDE)
                    hitKinE = hit.GetKinE()
                    HIT.SetKinE(hitKinE)
                    TRK.AddHit(HIT)
                G4.AddTrack(TRK)

            eve.Add(G4)
            mad.Add(MD)

def ConvertHist(h):
    if 'TH1' in str(type(h)):
        bc = []
        bv = []
        be = []
        for i in range(h.GetNbinsX()):
            bc.append(h.GetBinCenter(i+1))
            bv.append(h.GetBinContent(i+1))
            be.append(h.GetBinError(i+1)**2.0)
        hh = H1(1,1,1)
        title = h.GetTitle()
        xlabel = h.GetXaxis().GetTitle()
        ylabel = h.GetYaxis().GetTitle()
        hh.SetFromData(np.array(bc),np.array(bv),np.array(be),np.zeros(4,dtype=np.float64),title,xlabel,ylabel)
        # hh.SetTitle(title)
        # hh.SetXLabel(xlabel)
        # hh.SetYLabel(ylabel)
        return hh
    elif 'TH2' in str(type(h)):
        bv = []
        be = []
        nx = h.GetNbinsX()
        ny = h.GetNbinsY()
        bcx = np.linspace(h.GetXaxis().GetBinCenter(1),
            h.GetXaxis().GetBinCenter(nx),nx)
        bcy = np.linspace(h.GetYaxis().GetBinCenter(1),
            h.GetYaxis().GetBinCenter(ny),ny)
        for i in range(nx):
            for j in range(ny):
                bv.append(h.GetBinContent(i+1,j+1))
                be.append(h.GetBinError(i+1,j+1)**2.0)
        bv = np.array(bv).reshape(nx,ny)
        be = np.array(be).reshape(nx,ny)
        title = h.GetTitle()
        xlabel = h.GetXaxis().GetTitle()
        ylabel = h.GetYaxis().GetTitle()
        hh = H2(1,1,1,1,1,1)
        hh.SetFromData(bcx,bcy,bv,be,np.zeros(6,dtype=np.float64),title,xlabel,ylabel)
        # hh.SetTitle(title)
        # hh.SetXLabel(xlabel)
        # hh.SetYLabel(ylabel)
        return hh
    else:
        raise StandardError('Someone fucked up')
