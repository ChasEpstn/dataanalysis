import numpy as np
cimport numpy as np
from Vectors cimport ThreeVector,FourVector

cdef class G4Event(object):
    cdef public int eventID
    cdef public double weight
    cdef public str baseGeometry
    cdef public str generatorName
    cdef public list tracks
    cpdef int GetEventID(self)
    cpdef void SetEventID(self, int)
    cpdef double GetWeight(self)
    cpdef void SetWeight(self, double)
    cpdef void SetBaseGeometry(self,str)
    cpdef void SetGeneratorName(self, str)
    cpdef list GetTracks(self)
    cpdef void AddTrack(self,G4Track)
    cpdef int GetNumTracks(self)
    cpdef G4Track GetTrack(self, int)

cdef class G4Track(object):
    cdef public int trackID
    cdef public int parentTrackID
    cdef public tuple origin
    cdef public str particleName
    cdef public tuple momentumP4
    cdef public list hits
    cpdef int GetTrackID(self)
    cpdef void SetTrackID(self, int)
    cpdef str GetParticleName(self)
    cpdef void SetParticleName(self, str)
    cpdef int GetParentID(self)
    cpdef void SetParentID(self, int)
    cpdef ThreeVector GetOrigin(self)
    # cpdef void SetOrigin(self, tuple)
    cpdef ThreeVector Get3Momentum(self)
    cpdef FourVector Get4Momentum(self)
    # cpdef void Set4Momentum(self, tuple)
    cpdef list GetHits(self)
    cpdef void AddHit(self,G4Hit)

cdef class G4Hit(object):
    cdef public tuple position
    cdef public tuple localPosition
    cdef public tuple momentumP3
    cdef public double dE
    cdef public double kinE
    cdef public str detectorID
    cpdef str GetDetectorID(self)
    cpdef void SetDetectorID(self, str)
    cpdef ThreeVector GetPosition(self)
    # cpdef void SetPosition(self, tuple)
    cpdef ThreeVector GetLocalPosition(self)
    # cpdef void SetLocalPosition(self, tuple)
    cpdef ThreeVector Get3Momentum(self)
    # cpdef void Set3Momentum(self, tuple)
    cpdef double GetDE(self)
    cpdef void SetDE(self, double)
    cpdef double GetKinE(self)
    cpdef void SetKinE(self, double)
