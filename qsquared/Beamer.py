import os, glob, copy
from .Storage import *

class BeamerPresentation:
    def __init__ (self,name):
        self.name = name
        self.title = 'Presentation Title'
        self.author = 'Author'
        self.date = '\\today'
        self.imgFiles = []
        self.qFiles = []
        self.openQFiles = []
        self.slides = []
    def SetAuthor(self,author):
        self.author = author
    def SetDate(self,date):
        self.date = date
    def SetTitle(self,title):
        self.title = title
    def MakeSlide(self,dir,fName,title):
        fName = fName.split('.')
        # fName = '{'+fName+'}.pdf'
        s = ('\n\\begin{frame}{'+title+'}\n\includegraphics'
        '[width=\\textwidth]{{"'+dir+'/'+fName[0]+'"}.'+fName[1]+'}\n\end{frame}\n')
        return s
    def MakeDir(self):
        if not os.path.exists(self.name):
            os.makedirs(self.name)
    def AddFiles(self,files):
        assert isinstance(files,list)
        self.imgFiles += files
    def AddQFiles(self,qFiles):
        assert isinstance(files,list)
        self.qFiles += qFiles
    def AddFile(self,file):
        assert isinstance(file,str)
        self.imgFiles.append(file)
    def AddQFile(self,qFile):
        assert isinstance(qFile,str)
        self.qFiles.append(qFile)
    def AddOpenQF(self,QF):
        assert isinstance(QF,QFile)
        self.openQFiles.append(QF)
    def ExtractQHists(self):
        self.MakeDir()
        incNames = []
        for name in self.qFiles:
            incNames += glob.glob(name)
        for qname in incNames:
            self.openQFiles.append(QFile(qname,'r'))
        for qf in self.openQFiles:
            for tag in qf.Hists():
                hist = qf.ReadHist(tag)
                title = 'Slide Title'
                if hist.title:
                    title = hist.title#copy(hist.title)
                    hist.title = None
                hist.Draw(self.name+'/'+tag)
                self.slides.append(self.MakeSlide(os.getcwd()+'/'+self.name,tag,title))
    def ExtractHists(self):
        incNames = []
        for name in self.imgFiles:
            incNames += glob.glob(name)
        print "Including:"
        for n in incNames:
            print '\t'+n
        for name in incNames:
            self.slides.append(self.MakeSlide(os.getcwd(),name,'Slide Title'))
    def WriteSlides(self):
        self.MakeDir()
        with open(self.name+'/'+self.name+'.tex','w') as file:
            file.write(self.Header())
            self.ExtractHists()
            self.ExtractQHists()
            for slide in self.slides:
                file.write(slide)
            file.write(self.Footer())
    def CompileSlides(self):
        os.chdir(self.name)
        os.system('latexmk -pdf '+self.name+'.tex')
    def Header(self):
        header = """% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[11pt,compress]{beamer}
%\usepackage{pdflatex}
\usepackage{bibentry}
\usepackage{verbatim}
\usepackage{booktabs}
\usepackage{bm}
\usepackage{ulem}
\usepackage[abs]{overpic}
\usepackage{tcolorbox}
\usepackage{color}
% \usepackage{gnuplot-lua-tikz}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{upgreek}
\usepackage{cleveref}%[2012/02/15]% v0.18.4;
% 0.16.1 of May 2010 would be sufficient, but what is the exact day?

\crefformat{footnote}{#2\\footnotemark[#1]#3}

%\usepackage[style=authoryear,natbib=true,sortcites=true,block=space]{biblatex}
\usetheme[]{lownoise}

\useinnertheme{circles}
\setbeamertemplate{items}[square]

\\title[1B]{"""+self.title+"""}
\\author{"""+self.author+"""}
\date["""+self.date+"""]{"""+self.date+"""}

\\newcommand{\degrees}{\ensuremath{^\circ}}
\\newcommand{\la}{\ensuremath{\langle}}
\\newcommand{\\ra}{\ensuremath{\\rangle}}
\\newcommand{\Or}{\ensuremath{{\cal O}}}


\definecolor{Red}{rgb}{0.70,0,0}
\definecolor{dark}{rgb}{0.5,0,0}
\definecolor{darkblue}{rgb}{0,0,0.5}
\definecolor{teal}{RGB}{33, 124, 126}
\definecolor{red2}{RGB}{154, 51, 52}
\definecolor{dark2}{rgb}{0.35,0.1,0.1}%{0.4,0,0}
\definecolor{darkgray}{rgb}{0.2,0.2,0.2}
\definecolor{colbkg}{rgb}{0.95,0.95,0.95}%{gray!10}

\AtBeginSection[] {    % setup outline at start of each section
  \\begin{frame}[plain]
  	\\begin{columns}
		  \\begin{column}{0.65\\textwidth}
 {\Large Outline}
      		\\vspace{0.07\\textheight}
			 \\tableofcontents[currentsection]
		  \end{column}

		  \\begin{column}{0.265\\textwidth}
		    % \includegraphics[height=\paperheight]{FEL_sidebar.pdf}
		  \end{column}
		\end{columns}

  \end{frame}


  \\addtocounter{framenumber}{-1}
}

\\begin{document}
\maketitle\n\n"""
        return header
    def Footer(self):
        footer = "\n\n\end{document}\n"
        return footer
