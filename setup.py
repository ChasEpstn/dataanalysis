from setuptools import setup, Extension
from Cython.Build import cythonize
import numpy as np

extensions = [Extension("qsquared.HistsC",["qsquared/HistsC.pyx"],include_dirs=[np.get_include()])]

extensions.append(Extension("qsquared.Vectors",["qsquared/Vectors.pyx"],include_dirs=[np.get_include()]))
extensions.append(Extension("qsquared.G4Event",["qsquared/G4Event.pyx"],include_dirs=[np.get_include()]))

# palettes = glob('gnuplot-palettes-master/*')
# libs = glob('lib/*')
setup(
    name = 'qsquared',
    packages = ['qsquared'],
    version = '0.2',
    install_requires=[
    "numpy",
    "Cython",
    "ZODB",
    "zc.zlibstorage",
    "gnuplot-py"
    ],
    author = "Charles Epstein",
    author_email = "cepstein@mit.edu",
    description = "The qsquared data analysis package.",
    ext_modules = cythonize(extensions),
    package_data = {
        'qsquared' : ['color-palettes/*','lib/*']
    }
)


#run:
#python setup.py build_ext --inplace --force
