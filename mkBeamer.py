#!/usr/bin/env python

from qsquared import *
import sys

fName = sys.argv[1].split('.tex')[0]

bp = BeamerPresentation(fName)
bp.AddFiles(sys.argv[2::])
bp.SetAuthor('Charles Epstein')
bp.WriteSlides()
