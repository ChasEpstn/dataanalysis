# README #


### QSquared Data Analysis Software ###

This repository provides files for the python package 'QSquared'.  QSquared is a set of extensions to existing data analysis packages, on which it is dependent, including ROOT, zodb, numpy, and Gnuplot.  The extensions are intended to be useful for the average high-energy physics user, and include pythonic versions of objects like 3- and 4-vectors, 1D and 2D histograms, and some basic data structures and File IO.  The emphasis is on ease of use, speed, and storage efficiency.  Some highlights:

* Code acceleration via Cython compilation, when appropriate

* 1D and 2D histogram classes with C back-end

* Direct interface with Gnuplot: allows histograms to be saved easily and natively to a PDF

* Various data structures (Histograms, Series, Line, etc)

* Fast and space-efficient object storage via the ZODB database back-end and using zc.zlibstorage compression

* Easy conversion from ROOT histograms to qsquared histograms

* Easy conversion of ROOT files to qsquared QFiles; more and more ROOT object formats are being supported as time goes on

* Easy interface with ROOT trees, etc, and conversion to (faster) QFiles

* Uncertainties and units: value with unit (and error) classes; easy unit conversions. User can edit external units file.

* PDG information about elementary particles, easy to access their mass, width, etc. 

* Optional plotting Canvas with various automated plotting tools (which would take much coding in raw gnuplot)

* Various histogram color schemes available via [link here], default is veridis from matplotlib, which is grayscale- and colorblind-safe

* Beamer interface and utilities for generating slides out of plots made with qsquared


### How do I get set up? ###

* Unpack the distribution and run 'python setup.py install' (sudo probably required)

* The required dependencies are numpy, Cython, ZODB, zc.zlibstorage, and gnuplot-py.  For full functionality, ROOT (particularly, pyroot) should be installed, and latexmk for some additional capabilities.  

* The utility mkBeamer.py is included for convenience; it can be aliased as a command-line util to make Beamer presentations out of various image/pdf files.  

* Some examples are located in the examples/ directory

### Who do I talk to? ###

* Charles Epstein, cepstein (at) mit (dot) edu
